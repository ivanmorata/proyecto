package com.example.navigation.navegacion

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.navigation.NamedNavArgument
import androidx.navigation.navArgument

sealed class Destination(
    val ruta: String,
    val nombrePag: String,
    val icono: ImageVector,
    val pasarArgumentos: List<NamedNavArgument>
    ){

    //Splash screen -> Al arrancar app
    object PantallaSplaschScreen: Destination("splash_screen","Pantalla splash",Icons.Filled.Home, emptyList())

    //Login y registro
    object PantallaLogin: Destination("login_screen", "Pantalla 1", Icons.Filled.Home, emptyList())
    object PantallaRegistro: Destination("registro_screen","Register",Icons.Filled.Home, emptyList())


    //Peso y calorias
    object PantallaPeso: Destination("peso_screen","Pesos", Icons.Filled.BarChart, emptyList())
    object PantallaFormCalorias: Destination("calorias_screen", "Calorias", Icons.Filled.BarChart, emptyList())


    //Ejercicios
    object PantallaEjercicios: Destination("ejercicios_screen", "Ejercicio", Icons.Filled.FitnessCenter, emptyList())
    object PantallaEditEjercicios : Destination("editEjercicios_screen", "Pantalla edit ejercicios", Icons.Filled.FitnessCenter,
    pasarArgumentos = listOf(
        navArgument("id"){nullable = true} // este argumento podra ser nulo ya que esta pantalla tambien se usara para añadir nuevo ejercicio
    ))

    //Perfil
    object PantallaPerfil:Destination("perfil_screen","Inicio",Icons.Filled.Person, emptyList())

    //Notas
    object PantallaVerNotas:Destination("vernotas_screen","Notas",Icons.Filled.Notes, emptyList())
    object PantallaEditNotas:Destination("editNotas_screen","Notas",Icons.Filled.Notes, emptyList())

}
