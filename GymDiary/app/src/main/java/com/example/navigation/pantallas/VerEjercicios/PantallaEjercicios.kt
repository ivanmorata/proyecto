package com.example.navigation.pantallas

import android.annotation.SuppressLint
import android.widget.Toast
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import com.example.navigation.Repositorios.ConexionBBDD.Ejercicio.Ejercicio
import com.example.navigation.navegacion.Destination
import com.example.navigation.pantallas.Notas.ver.DialogoEmailerroneo
import com.example.navigation.pantallas.VerEjercicios.EjerciciosEstado
import com.example.navigation.pantallas.VerEjercicios.VerEjerciciosViewModel
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException


@OptIn(ExperimentalMaterialApi::class)
@Composable
fun PantallaEjercicios(
    navController: NavController,
    estado: EjerciciosEstado,
    EjercicioClick : (String) ->Unit,
    deleteEjercicio: (String) ->Unit,
    viewModel: VerEjerciciosViewModel?

    )

{
    Scaffold(topBar = {
        TopAppBar() {
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Ejercicios", fontSize = 18.sp)
        }

    }, floatingActionButton = {
        FloatingActionButton(
            onClick = {
                navController.navigate(Destination.PantallaEditEjercicios.ruta)
                      },
            backgroundColor = Color(0xFF2DB4CC),
            contentColor = Color.White,
            modifier = Modifier.padding(bottom = 55.dp)
        ) {
            Icon(imageVector = Icons.Default.Add, contentDescription = "")
        }
    })

    {
        ExercicesList(navController,estado, EjercicioClick,deleteEjercicio,viewModel)

    }
}



@SuppressLint("CoroutineCreationDuringComposition", "UnrememberedMutableState")
@ExperimentalMaterialApi
@Composable
fun ExercicesList(navController: NavController, estado: EjerciciosEstado,
                  EjercicioClick: (String) -> Unit,
                  deleteEjercicio: (String) -> Unit,
                  viewModel: VerEjerciciosViewModel?,
) {

    val listEjercicios: List<Ejercicio> by mutableStateOf(emptyList())

    val context = LocalContext.current
    //Al entrar en la pantalla
    LaunchedEffect(key1 = Unit){
        viewModel?.cargarEjercicios()
    }

    LaunchedEffect(key1 = viewModel?.usuarioIniciado) {
        if (viewModel?.usuarioIniciado == false) {
            navController.navigate(Destination.PantallaLogin.ruta)
        }
    }


    var ejercicioSeleccionado: Ejercicio? by remember { mutableStateOf(null) }
    var rutinaSeleccionada: MutableState<List<Ejercicio>?> = remember { mutableStateOf(emptyList()) }


    //Variables booleana que utilizamos para mostrar o no los distintos dialogos
    var verenviarEjercicio by rememberSaveable { mutableStateOf(false) }
    var verEnviarRutina by rememberSaveable { mutableStateOf(false) }
    var verConfirmacionEmail by rememberSaveable { mutableStateOf(false) }
    var verConfirmacionEmailRutina by rememberSaveable { mutableStateOf(false) }


    var emailTextt by remember { mutableStateOf("") }
    var idUsuarioEnvio by remember { mutableStateOf("") }

    LaunchedEffect(emailTextt) {
        val UsuarioEnvio = viewModel?.obtenerIdUsuarioPorEmail(emailTextt)
        idUsuarioEnvio = UsuarioEnvio ?: ""

    }

    var verEjerciciosVacios by rememberSaveable { mutableStateOf(false) }
    DialogoEjerciciosVacios(ver = verEjerciciosVacios, aceptar = { navController.navigate(Destination.PantallaEditEjercicios.ruta)},
        cancelar = {navController.navigate(Destination.PantallaPerfil.ruta)})

    if (estado.ejercicioMostrar.isEmpty()){
        verEjerciciosVacios = true
    } else{
        verEjerciciosVacios = false
    }


//Dialogo email erroneo
    var mensajeEmailErroneo by remember { mutableStateOf("") }

    var verEmailErroneo by rememberSaveable { mutableStateOf(false) }
    DialogoEmailerroneo(ver = verEmailErroneo, mensaje = mensajeEmailErroneo, aceptar = {verEmailErroneo = false})

    val verDesplegableEjercicios = remember { mutableStateMapOf<String, Boolean>() }



    LaunchedEffect(emailTextt) {
        val UsuarioEnvio =
            viewModel?.obtenerIdUsuarioPorEmail(emailTextt)
        idUsuarioEnvio = UsuarioEnvio ?: ""
        try {
            if (emailTextt != null && emailTextt.isNotEmpty()) {
                if (viewModel?.emailExiste(emailTextt) == false) {
                    verConfirmacionEmail = false
                    verEmailErroneo = true
                    mensajeEmailErroneo = "El usuario al que intenta enviar el ejercicio no se ha registrado en GymDiary."
                }
            }
        }catch (e: FirebaseAuthInvalidCredentialsException){
            verConfirmacionEmail = false
            verEmailErroneo = true
            mensajeEmailErroneo = "El formato del email al que intenta enviar es incorrecto."
        }

    }

    //Dialogo para enviar un ejercicio
    DialogoEnviarEjercicio(ver = verenviarEjercicio == true,
        aceptar = { emailText ->
            emailTextt = emailText
            verenviarEjercicio = false
            verConfirmacionEmail = true

        },
        cancelar = { verenviarEjercicio = false },
        mensaje = "Email del usuario al que quieras enviar el ejercicio",
        titulo = "Enviar ejercicio"

    )


    //Dialogo para enviar la  rutina de un dia
    DialogoEnviarEjercicio(ver = verEnviarRutina == true,
        aceptar = { emailText ->
            emailTextt = emailText
            verEnviarRutina = false
            verConfirmacionEmailRutina = true

        },
        cancelar = { verEnviarRutina = false },
        mensaje = "Email del usuario al que quieres enviar tu rutina completa del día",
        titulo = "Enviar rutina"

    )

    //Confirmacion envio rutina
    DialogoConfirmarEnvioEj(
        ver = verConfirmacionEmailRutina,
        aceptar = {
            if (emailTextt.isEmpty()) {
                verEmailErroneo = true
                mensajeEmailErroneo =
                    "El email no puede estar vacío."
                verConfirmacionEmailRutina = false
            } else if (emailTextt == viewModel?.nombre) {
                verEmailErroneo = true
                mensajeEmailErroneo =
                    "No puedes enviar una rutina a ti mismo."
                verConfirmacionEmailRutina = false
            } else {
                for (ej in rutinaSeleccionada.value!!){
                    viewModel?.envioEjercicio(

                        idUsuario = idUsuarioEnvio,
                        imagen = ej.imagen,
                        nombreEjercicio = ej.nombreEjercicio,
                        InfoEjercicio = ej.infoEjercicio,
                        series = ej.series,
                        diaSemana = ej.diaSemana,
                    )
            }

                Toast.makeText(
                    context,
                    "¡Rutina enviada!",
                    Toast.LENGTH_SHORT
                ).show()
                verConfirmacionEmailRutina = false
            }
        },
        cancelar = { verConfirmacionEmail = false },
        email = emailTextt
    )

    //Confirmacion envio ejericio
    DialogoConfirmarEnvioEj(
        ver = verConfirmacionEmail,
        aceptar = {
            if (emailTextt.isEmpty()) {
                verEmailErroneo = true
                mensajeEmailErroneo =
                    "El email no puede estar vacío."
                verConfirmacionEmail = false
            } else if (emailTextt == viewModel?.nombre) {
                verEmailErroneo = true
                mensajeEmailErroneo =
                    "No puedes enviar un ejercicio a ti mismo."
                verConfirmacionEmail = false
            } else {
                viewModel?.envioEjercicio(
                    idUsuario = idUsuarioEnvio,
                    imagen = ejercicioSeleccionado!!.imagen,
                    nombreEjercicio = ejercicioSeleccionado!!.nombreEjercicio,
                    InfoEjercicio = ejercicioSeleccionado!!.infoEjercicio,
                    series = ejercicioSeleccionado!!.series,
                    diaSemana = ejercicioSeleccionado!!.diaSemana,
                )
                Toast.makeText(
                    context,
                    "¡Ejercicio enviado!",
                    Toast.LENGTH_SHORT
                ).show()
                verConfirmacionEmail = false
            }
        },
        cancelar = { verConfirmacionEmail = false },
        email = emailTextt
    )

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(bottom = 55.dp)
    ){

            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .clip(shape = RoundedCornerShape(20.dp))
                    .padding(horizontal = 15.dp, vertical = 12.dp)

            ) {


                    val ejerciciosOrdenados = estado.ejercicioMostrar.sortedByDescending { it.color == 2 }

                    data class DiaSemana(val dia: String, val image:String)
                    val diaSemana = listOf(
                        DiaSemana("Lunes","https://cdn-icons-png.flaticon.com/512/5783/5783182.png"),
                        DiaSemana("Martes","https://cdn-icons-png.flaticon.com/512/5783/5783289.png"),
                        DiaSemana("Miércoles","https://cdn-icons-png.flaticon.com/512/5783/5783306.png"),
                        DiaSemana("Jueves","https://cdn-icons-png.flaticon.com/512/5783/5783260.png"),
                        DiaSemana("Viernes","https://cdn-icons-png.flaticon.com/512/5783/5783145.png"),
                        DiaSemana("Sábado","https://cdn-icons-png.flaticon.com/512/5783/5783224.png"),
                        DiaSemana("Domingo","https://cdn-icons-png.flaticon.com/512/5783/5783248.png")
                    )

                    items(diaSemana) { dia ->


                        val ejerciciosPorDia = estado.ejercicioMostrar.filter { it.diaSemana == dia.dia }.isNotEmpty()

                        val ejerciciosPendientes = estado.ejercicioMostrar.any { it.diaSemana == dia.dia && it.color == 2 }

                        val colorFondo = if (ejerciciosPendientes) {
                            Color(0xFFE19630)
                        } else if (ejerciciosPorDia) {
                            Color(0xFF2DB4CC)

                        } else {
                            Color(0xFFBDBDBD)
                        }

                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .clip(shape = RoundedCornerShape(20.dp))
                                .background(color = colorFondo)
                                .padding(horizontal = 8.dp, vertical = 7.dp)
                                .clickable {
                                    verDesplegableEjercicios[dia.dia] =
                                        !(verDesplegableEjercicios[dia.dia] ?: false)
                                    if (estado.ejercicioMostrar
                                            .filter { it.diaSemana == dia.dia }
                                            .isEmpty()
                                    ) {
                                        Toast
                                            .makeText(
                                                context,
                                                "Todavía no tienes ejercicios el ${dia.dia}",
                                                Toast.LENGTH_LONG
                                            )
                                            .show()
                                    }

                                }

                        ) {
                            Row(
                                verticalAlignment = Alignment.CenterVertically,
                                //horizontalArrangement = Arrangement.SpaceBetween,
                                //modifier = Modifier.fillMaxWidth()
                            ) {

                                Image(
                                    painter = rememberImagePainter(dia.image),
                                    contentDescription = "",
                                    modifier = Modifier.size(55.dp)
                                )
                                Spacer(modifier = Modifier.width(10.dp))

                                Column(modifier = Modifier.weight(1f).fillMaxWidth())
                                {
                                    Text(
                                        text = dia.dia,
                                        style = MaterialTheme.typography.h5,
                                       // modifier = Modifier.padding(start = 10.dp)
                                    )
                                }



                                IconButton(
                                    onClick = {
                                        val ejerciciosDelDia = estado.ejercicioMostrar.filter { it.diaSemana == dia.dia }
                                        verEnviarRutina = true
                                        rutinaSeleccionada.value = ejerciciosDelDia
                                    }
                                ) {
                                    Icon(imageVector = Icons.Filled.ReplyAll, contentDescription = "")
                                }


                                    Icon(
                                        imageVector = if (verDesplegableEjercicios[dia.dia] == true && estado.ejercicioMostrar.filter { it.diaSemana == dia.dia }.isNotEmpty())
                                            Icons.Default.ExpandLess else
                                                Icons.Default.ExpandMore,
                                        contentDescription = ""
                                    )

                            }

                        }

                        Spacer(modifier = Modifier.height(8.dp)) // Espacio vertical entre cada Box

                        if (verDesplegableEjercicios[dia.dia] == true) {
                            val ejerciciosPorDia =
                                ejerciciosOrdenados.filter { it.diaSemana == dia.dia }

                            ejerciciosPorDia.forEach { ejercicio ->

                                var eliminarEjercicio by remember { mutableStateOf(false) }
                                val estadoBorrado = rememberDismissState(
                                    confirmStateChange = { borrado ->
                                        if (borrado == DismissValue.DismissedToEnd)
                                            eliminarEjercicio = !eliminarEjercicio
                                        borrado != DismissValue.DismissedToEnd
                                    }
                                )

                                //en esta pantalla utilizaremos el swipe para borrar los ejercicios de firebase
                                SwipeToDismiss(
                                    state = estadoBorrado,
                                    directions = setOf(DismissDirection.StartToEnd),
                                    dismissThresholds = {
                                        FractionalThreshold(0.6f)
                                    },
                                    background = {
                                        val dir =
                                            estadoBorrado.dismissDirection ?: return@SwipeToDismiss
                                        val color by animateColorAsState(
                                            when (estadoBorrado.targetValue) {
                                                DismissValue.Default -> Color.Red
                                                DismissValue.DismissedToEnd -> Color.Red
                                                DismissValue.DismissedToStart -> Color.Red
                                            }
                                        )
                                        val alinear = when (dir) {
                                            DismissDirection.StartToEnd -> Alignment.CenterStart
                                            DismissDirection.EndToStart -> Alignment.CenterEnd
                                        }
                                        val icono = when (dir) {
                                            DismissDirection.StartToEnd -> Icons.Default.Delete
                                            DismissDirection.EndToStart -> Icons.Default.Delete
                                        }
                                        val escala by animateFloatAsState(
                                            if (estadoBorrado.targetValue == DismissValue.Default) 0.85f else 1f
                                        )

                                        Box(
                                            Modifier
                                                .fillMaxSize()
                                                .background(color)
                                                .padding(horizontal = 20.dp),
                                            contentAlignment = alinear
                                        ) {
                                            Icon(
                                                icono,
                                                contentDescription = "",
                                                modifier = Modifier.scale(escala)
                                            )
                                        }
                                    }
                                ) {

                                    if (eliminarEjercicio) {
                                        AlertDialog(
                                            onDismissRequest = { eliminarEjercicio = false },
                                            title = { Text("Eliminar ejercicio") },
                                            text = { Text("¿Estás seguro de que deseas eliminar este ejercicio?") },
                                            confirmButton = {
                                                TextButton(onClick = {
                                                    deleteEjercicio(ejercicio.id)
                                                    eliminarEjercicio = false
                                                    Toast.makeText(
                                                        context,
                                                        "Ejercicio eliminado",
                                                        Toast.LENGTH_SHORT
                                                    ).show()

                                                }) {
                                                    Text("Confirmar")
                                                }
                                            },
                                            dismissButton = {
                                                TextButton(onClick = {
                                                    eliminarEjercicio = false
                                                }) {
                                                    Text("Cancelar")
                                                }
                                            }
                                        )

                                    } else {
                                        CardEjercicios(ejercicio,
                                            EjercicioClick,
                                            EnviarEjercicio = {
                                                verenviarEjercicio = true
                                                ejercicioSeleccionado = ejercicio
                                            },
                                            cambiarcolor = {
                                                viewModel?.updateColorEjercicio(
                                                    ejercicio.id,
                                                    2
                                                )
                                            },
                                            cambiarcolor2 = {
                                                viewModel?.updateColorEjercicio(ejercicio.id, 1)

                                            }
                                        )
                                    }


                                }
                            }

                        }
                    }


                }




        if(estado.error.isNotBlank()){
            Text(
                modifier = Modifier
                    .padding(horizontal = 20.dp)
                    .fillMaxWidth()
                    .align(Alignment.Center),
                text = estado.error,
                textAlign = TextAlign.Center,
                color = Color.Red,
            )
        }

        if(estado.cargando) {
            CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
        }
    }
}



@OptIn(ExperimentalFoundationApi::class)
@SuppressLint("SuspiciousIndentation")
@Composable
fun CardEjercicios(ejercicio: Ejercicio, EjercicioClick: (String) -> Unit,
                   //ejercicios: List<Ejercicio>,
                   EnviarEjercicio: () ->Unit, cambiarcolor: ()->Unit, cambiarcolor2: ()->Unit ) {

    var colorr:Color = Color.White
    var textColor by remember { mutableStateOf("") }

    if (ejercicio.color == 1){
        textColor = "REALIZADO"
        colorr = Color(0xFF7BE367)

    }else if (ejercicio.color == 2) {
        textColor = "PENDIENTE"
        colorr = Color(0xFFE19630)

    }



    Card(
        elevation = 0.dp, //shape = RoundedCornerShape(20.dp),
        modifier = Modifier
            .padding(8.dp)
            .combinedClickable(onClick = { EjercicioClick(ejercicio.id) })
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()

        ) {
            Image(
                painter = rememberImagePainter(ejercicio.imagen),
                contentDescription = "",
                modifier = Modifier
                    .background(color = Color.White)
                    .height(203.dp)
                    .width(140.dp)
                    .padding(8.dp)
            )

            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(vertical = 8.dp, horizontal = 9.dp)
                    .weight(1f),
                verticalArrangement = Arrangement.spacedBy(12.dp),



            ) {
                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = ejercicio.nombreEjercicio,
                    style = TextStyle(
                        fontWeight = FontWeight.Medium,
                        fontSize = 19.sp,
                    )
                )

                Text(
                    modifier = Modifier.fillMaxWidth(),
                    text = ejercicio.infoEjercicio,
                    style = TextStyle(
                        fontWeight = FontWeight.Light,
                        fontSize = 14.sp,
                        color = Color.DarkGray
                    ),
                    maxLines = 3,
                   overflow = TextOverflow.Ellipsis

                )


                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.CenterHorizontally),
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically
                ) {

                    Text(
                        text = "${ejercicio.series} series",
                        style = TextStyle(
                            color = Color.Black,
                            fontWeight = FontWeight.Black,
                            fontSize = 14.sp
                        )
                    )
                    Spacer(modifier = Modifier.width(8.dp))


                    if (ejercicio.color == 2){
                        Button(
                            onClick = {cambiarcolor2()},//{EjercicioClick(ejercicio.id)},
                            shape = RoundedCornerShape(80),
                            colors = ButtonDefaults.buttonColors(
                                backgroundColor = Color(0xFF7BE367)
                            )
                        ) {
                            Row(verticalAlignment = Alignment.CenterVertically) {
                                Icon(Icons.Default.Check, contentDescription = "Marcar como realizado", tint = Color.White)
                            }
                        }
                    } else {
                        Button(
                            onClick = { cambiarcolor() },
                            shape = RoundedCornerShape(80),
                            colors = ButtonDefaults.buttonColors(
                                backgroundColor = Color.Red
                            )
                        ) {
                            Row(verticalAlignment = Alignment.CenterVertically) {
                                Icon(
                                    Icons.Default.PendingActions,
                                    contentDescription = "Marcar como pendiente",
                                    tint = Color.White
                                )
                            }
                        }
                    }

                    Spacer(modifier = Modifier.width(8.dp))



                    Button(
                        onClick = {EnviarEjercicio()},
                        shape = RoundedCornerShape(80),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = Color.Blue
                        )
                    ) {
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            Icon(Icons.Default.Send, contentDescription = "Enviar", tint = Color.White)
                        }
                    }

                }



                Box(modifier = Modifier
                    .fillMaxWidth()
                    .background(colorr, shape = RoundedCornerShape(20.dp))
                ) {
                    Text(
                        text = textColor,
                        modifier = Modifier.fillMaxWidth(),
                        style = TextStyle(
                            fontWeight = FontWeight.Bold,
                            textAlign = TextAlign.Center,
                            fontSize = 18.sp,
                        )
                    )
                }
            }
        }
    }
}


@Composable
fun DialogoEnviarEjercicio(ver: Boolean, aceptar: (String) -> Unit, cancelar: () -> Unit,mensaje:String,titulo:String){
    if (ver) {
        var emailText by remember { mutableStateOf("") }

        AlertDialog(
            onDismissRequest = { cancelar() },
            confirmButton = {
                Button(
                    onClick = { aceptar(emailText) },
                ) {
                    Text(text = "Aceptar")
                }
            },
            dismissButton = {
                Button(
                    onClick = { cancelar() },
                ) {
                    Text(text = "Cancelar")
                }
            }, text = {

                Column {
                    Text(text = mensaje)

                    Spacer(modifier = Modifier.height(25.dp)) // Agrega un espacio horizontal de 8dp

                    TextField(
                        value = emailText,
                        onValueChange = { emailText = it },

                        label = { Text("Email")
                        },
                        colors = TextFieldDefaults.textFieldColors(
                            backgroundColor = Color.Transparent,
                            focusedIndicatorColor = MaterialTheme.colors.primary,
                            unfocusedIndicatorColor = MaterialTheme.colors.primary
                        ),
                    )
                }


            }, title = {
                Text(text = titulo,
                    style = TextStyle(
                        fontSize = 22.sp,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Center,
                        )
            )}


        )
    }
}
@Composable
fun DialogoEjerciciosVacios(ver: Boolean,aceptar: () -> Unit,cancelar: () -> Unit){
    if (ver){
        AlertDialog(onDismissRequest = {},
            confirmButton ={
                TextButton(onClick = { aceptar() }) {
                    Text(text = "Empezar")
                }
            },
            dismissButton = {
                TextButton(onClick = { cancelar() }) {
                    Text(text = "Volver a inicio")
                }
            },
            title = { Text(text = "Registro de ejercicios vacío") },
            text = { Text(text = "Empieza a guardar los ejercicios que realizas en el gimnasio o prepara los ejercicios para enviárselos a otros usuarios de la app.\n\nPodrás cambiar el estado de estos a Realizado/Pendiente y borrarlos haciendo un swipe hacía la derecha.\n\n¡Recuerda mantener tus ejercicios actualizados!") },

            )
    }
}

@Composable
fun DialogoConfirmarEnvioEj(ver:Boolean,aceptar: () -> Unit,cancelar: () -> Unit,email:String) {
    if (ver) {
        AlertDialog(
            onDismissRequest = { cancelar() },
            confirmButton = {

                Button(
                    onClick = { aceptar() },
                ) {
                    Text(text = "Enviar")
                }
            },
            dismissButton = {
                Button(
                    onClick = { cancelar() },
                ) {
                    Text(text = "Cancelar")
                }
            },
            text = {
                Text(text = "¿Estás seguro que quieres enviar tu ejercicio a ${email}?")
            }


        )

    }
}
















