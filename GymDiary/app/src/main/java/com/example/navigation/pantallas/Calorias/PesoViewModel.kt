package com.example.navigation.pantallas.Calorias

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.navigation.Repositorios.ConexionBBDD.Peso.RepositoryPeso
import com.example.navigation.Repositorios.ConexionBBDD.Peso.ResultadoPeso
import com.google.firebase.Timestamp
import kotlinx.coroutines.launch

class PesoViewModel( private val repositoryPeso: RepositoryPeso = RepositoryPeso()
): ViewModel() {

    //Firebase authentication
    val usuario = repositoryPeso.usuario()
    val usuarioIniciado:Boolean
    get () = repositoryPeso.usuarioIiniciado()

    private val idUsuario:String
    get() = repositoryPeso.obtenerIdUsuario()


    //Clases ver / añadir pesos
    var pesoEstado by mutableStateOf(PesoEstado())
        private set

    var nuevoPesoEstado by mutableStateOf(NuevoPesoEstado())
    private  set


    fun cambioPeso(peso:String){
        nuevoPesoEstado = nuevoPesoEstado.copy(peso =  peso)
    }

    fun cambioFecha(fecha:String){
        nuevoPesoEstado = nuevoPesoEstado.copy(fecha = fecha)
    }


    //Funciones insert,cargar,delete
    fun cargarPesos(){
        if (usuarioIniciado){
            if (idUsuario.isNotBlank()){
                obtenerPesosdeUsuario(idUsuario)
            }
        }else {
            pesoEstado = pesoEstado.copy(listaPeso = ResultadoPeso.Error(
                throwable = Throwable(message = "Usuario no ha iniciado sesion")
            ))
        }
    }

    //solo obtendremos los pesos del  del usuario que ha iniciado sesion -> IdUsuario
    fun obtenerPesosdeUsuario(idUsuario:String) = viewModelScope.launch {
        repositoryPeso.obtenerPesosUsuario(idUsuario).collect{
            pesoEstado = pesoEstado.copy(listaPeso = it)
        }
    }

    //funciones de insertar y eliminar
    fun insertPeso(){
        if(usuarioIniciado){
            repositoryPeso.insertPeso(idUsuario = usuario!!.uid,
                peso = nuevoPesoEstado.peso,
                fecha = nuevoPesoEstado.fecha,
                creacion = Timestamp.now()){
                nuevoPesoEstado = nuevoPesoEstado.copy(estadoPesoNueva = it)
            }
        }
    }

    fun deletePeso(idPeso:String) = repositoryPeso.deletePeso(idPeso){
        pesoEstado = pesoEstado.copy(borrarPesoEstado = it)
    }


    fun TextPesoReset(){
        nuevoPesoEstado = NuevoPesoEstado()
    }
}