package com.example.navigation.pantallas.Notas.addEdit

import android.net.Uri
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import com.example.navigation.Repositorios.ConexionBBDD.Notas.Notas
import com.example.navigation.Repositorios.ConexionBBDD.Notas.RepositoyNotas
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseUser

class EditNotasViewModel(
    private val respositoryNotas : RepositoyNotas = RepositoyNotas()
) : ViewModel (){

    private val usuarioIniciado:Boolean
        get() = respositoryNotas.usuarioIiniciado()

    private val usuario:FirebaseUser?
        get() = respositoryNotas.usuario()




    var notaEstado by mutableStateOf(EditarNotasEstado())
    private set


    fun cambioImagenUri(uri: Uri?){
        notaEstado = notaEstado.copy(imagenUri = uri)
    }

    fun cambioVideoUri(uri: Uri?){
        notaEstado = notaEstado.copy(videoUri = uri)
    }

    fun cambioColor(color:Int){
        notaEstado = notaEstado.copy(color = color)
    }

    fun cambioTitulo(titulo:String){
        notaEstado = notaEstado.copy(titulo = titulo)
    }

    fun cambioinfoNota(infoNota:String){
            notaEstado = notaEstado.copy(infoNota = infoNota)
    }



    //Funciones insert, update, delete y obtener nota para actualizacion por Idnota
    fun insertNota() {
        if (usuarioIniciado) {
            if (notaEstado.imagenUri != null) { // si tenemos imagen agregamos los datos de imagen
                respositoryNotas.insertNota(
                    idUsuario = usuario!!.uid,
                    titulo = notaEstado.titulo,
                    infoNota = notaEstado.infoNota,
                    color = notaEstado.color,
                    creacion = Timestamp.now(),
                    imagenUri = notaEstado.imagenUri,
                    videoUri = null
                ) {
                    notaEstado = notaEstado.copy(estadoNotaNueva = it)

                }
            } else if (notaEstado.videoUri != null){
                respositoryNotas.insertNota(
                    idUsuario = usuario!!.uid,
                    titulo = notaEstado.titulo,
                    infoNota = notaEstado.infoNota,
                    color = notaEstado.color,
                    creacion = Timestamp.now(),
                    imagenUri = null,
                    videoUri = notaEstado.videoUri
                ) {
                    notaEstado = notaEstado.copy(estadoNotaNueva = it)

                }
            }

            else {
                respositoryNotas.insertNota(
                    idUsuario = usuario!!.uid,
                    titulo = notaEstado.titulo,
                    infoNota = notaEstado.infoNota,
                    color = notaEstado.color,
                    creacion = Timestamp.now(),
                    imagenUri = null,
                    videoUri = null
                ) {
                    notaEstado = notaEstado.copy(estadoNotaNueva = it)

                }
            }
        }
    }


    fun editarNota(notas: Notas){
        val imagenUrl = Uri.parse(notas.imagenUrl)
        val videoUri = Uri.parse(notas.videoUrl)
        notaEstado = notaEstado.copy(
            color = notas.color,
            infoNota = notas.infoNota,
            titulo = notas.titulo,
            imagenUri = imagenUrl,
            videoUri = videoUri

        )
    }

    fun obtenerNota(idNota:String){
        respositoryNotas.obtenerNota(idNota = idNota, error = {},){
            notaEstado  = notaEstado.copy( notaSeleccionado = it)
            notaEstado.notaSeleccionado?.let { it1 -> editarNota(it1) }

        }
    }



    fun updateNota(IdNota: String) {

        respositoryNotas.updateNota(
            titulo = notaEstado.titulo,
            infoNota = notaEstado.infoNota,
            color = notaEstado.color,
            idNota = IdNota
        ) { success ->
            notaEstado = notaEstado.copy(estadoNotaActualz = success)
        }
    }


     fun estadoNota(){
         notaEstado = notaEstado.copy( estadoNotaActualz = false, estadoNotaNueva = false)
     }


    fun TextNotasReset(){
        notaEstado = EditarNotasEstado()
    }

}


