package com.example.navigation.pantallas.Login

import android.content.Context
import android.widget.Toast
import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.navigation.Repositorios.ConexionBBDD.Usuarios.RepositoryUsuarios
import kotlinx.coroutines.launch



class LoginViewModel(
    private val repositoryUsuarios: RepositoryUsuarios = RepositoryUsuarios()

) : ViewModel() {


        // Firebase Authentication
    val usuario = repositoryUsuarios.usuario

    val usuarioIniciado: Boolean
        get() = repositoryUsuarios.usuarioiniciado()

//Controlar distintos estados de la pantalla
    var loginEstado by mutableStateOf(LoginEstado())
        private set


    //Intermediario de valores entre la pantalla y el viewmodel
    fun cambionombre(nombreUsuario: String) {
        loginEstado = loginEstado.copy(nombre = nombreUsuario)
    }

    fun cambioContra(contra: String) {
        loginEstado = loginEstado.copy(contra = contra)
    }

    fun cambionombreRegistro(nombreUsuario: String) {
        loginEstado = loginEstado.copy(nombreRegistro = nombreUsuario)
    }

    fun cambiocontraRegistro(contra: String) {
        loginEstado = loginEstado.copy(contraRegistro = contra)
    }

    fun cambiocontraConfirm(contra: String) {
        loginEstado = loginEstado.copy(confirmarContra = contra)
    }



    //Funciones de validacion para el registro y login
     fun validarLogin() =
        loginEstado.contra.isNotBlank() && loginEstado.nombre.isNotBlank()

     fun camposEnBlanco() =
        loginEstado.contraRegistro.isBlank() && loginEstado.confirmarContra.isBlank() &&
                loginEstado.nombreRegistro.isBlank()

     fun minimo6()= loginEstado.contra.length <6 && loginEstado.confirmarContra.length<6

    fun ContraDiferente()=loginEstado.confirmarContra != loginEstado.contraRegistro


//Nuevo usuario + iniciar sesion

    fun crearUsuario(context: Context) = viewModelScope.launch {

        try {
            loginEstado = loginEstado.copy(cargando = true)
            loginEstado = loginEstado.copy(RegistroError = "")

            repositoryUsuarios.crearUsuario(loginEstado.nombreRegistro,
                loginEstado.contraRegistro)
            { correcto ->
                if (correcto) {
                    //Ha sido correcto el registro en Athentication, insertamos el usuario en Firestores firebase
                    val idUsuario = repositoryUsuarios.obtenerIdUsuario()
                    val email = loginEstado.nombreRegistro
                    repositoryUsuarios.insertarUsuario(idUsuario,email){} //insert collection usuarios
                    Toast.makeText(context, "Registro correcto", Toast.LENGTH_SHORT).show()
                    loginEstado = loginEstado.copy(LoginCoreccto = true)
                } else {
                    Toast.makeText(context, "Registro incorrecto", Toast.LENGTH_SHORT).show()
                    loginEstado = loginEstado.copy(LoginCoreccto = false)
                }
            }

        } catch (e: Exception) {
            Toast.makeText(context, "Registro incorrecto", Toast.LENGTH_SHORT).show()
            //Controlamos los errores directamente con los mensajes de Firebase
            loginEstado = loginEstado.copy(RegistroError = e.localizedMessage)
        } finally {
            loginEstado = loginEstado.copy(cargando = false) //Para que deje el Circular progress bar de dar vueltas
        }
    }


    fun iniciarSesion(context: Context) = viewModelScope.launch {

        try {
            loginEstado = loginEstado.copy(cargando = true)
            loginEstado = loginEstado.copy(LoginError = "")

            repositoryUsuarios.iniciarSesion(loginEstado.nombre, loginEstado.contra) { correcto ->
                if (!correcto) { // controlamos los mensaje que se veran por pantalla
                    Toast.makeText(context, "Error al iniciar sesion", Toast.LENGTH_SHORT).show()
                    loginEstado = loginEstado.copy(LoginCoreccto = false)
                } else {
                    Toast.makeText(context, "Sesion iniciada", Toast.LENGTH_SHORT).show()
                    loginEstado = loginEstado.copy(LoginCoreccto = true)
                }
            }

        } catch (e: Exception) {
            Toast.makeText(context, "Error al iniciar sesion", Toast.LENGTH_SHORT).show()
            loginEstado = loginEstado.copy(LoginError = e.localizedMessage)
        } finally {
            loginEstado = loginEstado.copy(cargando = false) //Acabe el Circular progress de dar vueltas
        }
    }

}





