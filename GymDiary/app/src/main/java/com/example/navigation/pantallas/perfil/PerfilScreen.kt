package com.example.navigation.pantallas.perfil

import android.app.Activity
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.navigation.navegacion.Destination
import com.example.navigation.pantallas.Notas.ver.VerNotasViewModel
import com.example.navigation.pantallas.VerEjercicios.VerEjerciciosViewModel
import com.example.navigation.R


@Composable
fun PantallaPerfil(navController: NavController, viewModel: VerNotasViewModel, viewModelEj: VerEjerciciosViewModel?) {

    val activity = (LocalContext.current as? Activity)

    //Dialogo de cerrar sesion
    var verCerrarSesion by rememberSaveable { mutableStateOf(false) }
    var verInfo by rememberSaveable { mutableStateOf(false) }

    DialogoCerrarSesion(ver = verCerrarSesion, aceptar = {
        verCerrarSesion = false
        viewModel.cerrarSesion()
        activity?.finish()

    },

        cancelar = { verCerrarSesion =false})

    DialogoInfo(ver = verInfo, aceptar = {verInfo = false})


    LaunchedEffect(key1 = viewModel?.usuarioIniciado) {
        if(viewModel?.usuarioIniciado){
            if (viewModel?.usuarioIniciado == false) {
                navController.navigate(Destination.PantallaLogin.ruta)
            }
        }
    }



    Scaffold(
        topBar = {
            TopAppBar() {
                Spacer(modifier = Modifier.width(8.dp))
                Text(text = "Inicio", fontSize = 18.sp)
                Spacer(modifier = Modifier.width(250.dp))
                IconButton(onClick = {
                        verInfo= true
                }) {
                    Icon(imageVector = Icons.Default.Info, contentDescription = null,)
                }
                IconButton(onClick = {
                    verCerrarSesion = true
                }) {
                    Icon(imageVector = Icons.Default.Logout, contentDescription = null,)
                }
            }
        }
    ) {
        Perfil(navController, viewModel, viewModelEj)

    }
}



@Composable
 fun Perfil(navController: NavController, viewModel: VerNotasViewModel, viewModelEj: VerEjerciciosViewModel? ){

    //Obtenemos el nombre del usuario hasta antes del arroba
    val nombreSinEmail = viewModel.nombre.toString().substringBefore('@')


    val listaAcceso : List<listaAcceso> = listOf(
        listaAcceso("Añadir ejercicio", R.drawable.gymcolor,"Añade los ejercicios que haces en el gimnasio y mantenlos actualizados para seguir todo tu progreso.\n\nTambién prodrás actuar como entrenador creando una rutina y enviándosela a otro usuario de GymDiary."),
        listaAcceso("Nueva nota", R.drawable.notavacia,"Añade notas sobre tu vida fitness, podrás incluir una foto o video a tu nota y enviarlas a otros usuarios de GymDiary."),
        listaAcceso("Agregar peso", R.drawable.bascula,"Agrega tus pesos diariamente vinculándolos a una fecha."),
        listaAcceso("Calculadora de calorías", R.drawable.fotocalculadoracalorias,"La calculadora de calorías te devolverá el número de calorías a seguir según tus datos y objetivos, podrás añadirlo a notas para hacer un seguimiento de tus comidas. "),
    )

    val context = LocalContext.current
    LaunchedEffect(Unit){
        viewModelEj?.verificarEjerciciosPendiente(context)
    }


    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center
    ) {

        LazyColumn(
            modifier = Modifier.padding(bottom = 56.dp),
            contentPadding = PaddingValues(15.dp),
            verticalArrangement = Arrangement.spacedBy(5.dp)
        ){

            item {
                Column() {
                    Text(
                        text = "Hola,",
                        style = MaterialTheme.typography.h3.copy(
                            fontWeight = FontWeight.Black,
                        )
                    )
                    Text(
                        text = nombreSinEmail,
                        style = MaterialTheme.typography.h4.copy(
                            fontWeight = FontWeight.Black,
                        )
                    )
                }

                Spacer(modifier = Modifier.height(15.dp))


                Image(
                    painter = painterResource(R.drawable.logo2),
                    contentDescription = "",
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .fillMaxWidth()
                )

                Spacer(modifier = Modifier.height(15.dp))

            }

            items(listaAcceso){
                    item ->
                Spacer(modifier = Modifier.height(5.dp))

                ListItemRow(item, navController)
            }
        }

        Spacer(modifier = Modifier.height(56.dp))

    }
}


class listaAcceso (var texto:String, var imagen:Int, var info:String)




@Composable
fun ListItemRow(lista: listaAcceso, navController: NavController) {
    var verr by remember { mutableStateOf(false) }

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .clip(shape = RoundedCornerShape(30.dp))
            .background(color = Color(0xFF2DB4CC))
            .padding(horizontal = 16.dp, vertical = 10.dp)
    ) {
        Row(verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.clickable {
                when (lista.texto) {
                    "Añadir ejercicio" -> navController.navigate(Destination.PantallaEditEjercicios.ruta)
                    "Nueva nota" -> navController.navigate(Destination.PantallaEditNotas.ruta)
                    "Agregar peso" -> navController.navigate(Destination.PantallaPeso.ruta)
                    "Calculadora de calorías" -> navController.navigate(Destination.PantallaFormCalorias.ruta)
                }
            }
        )

        {
            Image(
                painter = painterResource(lista.imagen),
                contentDescription = lista.info,
                modifier = Modifier.size(28.dp)
            )
            Spacer(modifier = Modifier.width(20.dp))
            
            Column(modifier = Modifier
                .weight(1f)
                .fillMaxWidth()
            )

            {
                Text(
                    text = lista.texto,
                    style = MaterialTheme.typography.body2,
                    color = Color.Black
                )

                if (verr){
                    Spacer(modifier = Modifier.height(4.dp))

                    Text(
                    text = lista.info,
                    //fontSize = 15.sp ,
                    style = MaterialTheme.typography.overline
                )
                }
            }

            IconButton(
                onClick = { verr = !verr }
            ) {
                Icon(imageVector = if (verr == false){
                    Icons.Default.ExpandMore
                }else {
                    Icons.Default.ExpandLess
                }
                    , contentDescription = null
                )

            }

        }
    }
}


@Composable
fun DialogoInfo(ver: Boolean, aceptar: () -> Unit){
    if (ver){
        AlertDialog(onDismissRequest = {},
            confirmButton = {Button(
                onClick = { aceptar() },
            ) {
                Text(text = "Aceptar")
            }},
            title = { Text(text = "Sobre nosotros") } ,
            text = { Text(text = "La aplicación GymDiary pertenece al TFG del grado superior de Desarrollo de aplicaciones Multiplataforma.\n\nRealizado por Iván Morata Fernández para el Colegio Salesiano Santo Domingo Savio.\n\nVersión 1.0") }
        )
    }

}


@Composable
fun DialogoCerrarSesion(ver:Boolean,aceptar:() ->Unit, cancelar :()->Unit){
    if (ver){
        AlertDialog(onDismissRequest = { cancelar() },
            confirmButton = {Button(
                onClick = { aceptar() },
            ) {
                Text(text = "Cerrar sesión y salir")
            } },
            dismissButton = {Button(
                onClick = { cancelar() },
            ) {
                Text(text = "Cancelar")
            }},
            title = { Text(text = "Cerrar Sesión") },
            text = { Text(text = "¿Quieres cerrar sesión en GymDiary?")}
        )
    }

}





