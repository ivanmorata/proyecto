package com.example.navigation.Repositorios.ConexionBBDD.Ejercicio

import com.google.firebase.Timestamp


data class Ejercicio (
    val idUsuario: String,
    val id: String,
    val imagen: String,
    val nombreEjercicio: String,
    val infoEjercicio: String,
    val diaSemana: String,
    val series: String,
    var color: Int,
    val timestamp: com.google.firebase.Timestamp = Timestamp.now(),

){
    constructor(
    ) : this("","","","","","","",0)

}