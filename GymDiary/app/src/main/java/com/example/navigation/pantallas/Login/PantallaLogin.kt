package com.example.navigation.pantallas

import android.content.Context
import android.view.inputmethod.InputMethodManager
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Key
import androidx.compose.material.icons.outlined.Mail
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.navigation.navegacion.Destination
import com.example.navigation.pantallas.Login.LoginViewModel
import com.example.navigation.R


@Composable
fun PantallaLogin(viewModel: LoginViewModel?, navController: NavController) {

    val loginEstado = viewModel?.loginEstado
    val context = LocalContext.current

    val mensajeError = loginEstado?.LoginError

    LaunchedEffect(key1 = viewModel?.usuarioIniciado){
        if (viewModel?.usuarioIniciado == true){
            navController.navigate(Destination.PantallaPerfil.ruta){
                popUpTo(Destination.PantallaLogin.ruta) {inclusive = true}
            }
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center

    ) {

        //Cabecera
        Text(
            text = "Iniciar sesión",
            style = MaterialTheme.typography.h3,
            fontWeight = FontWeight.Black,
            color = MaterialTheme.colors.primary

        )

        //Espacio
        Spacer(modifier = Modifier.height(15.dp))

        //Imagen
        Image(painter = painterResource(R.drawable.logo),
            contentDescription ="",
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .fillMaxWidth()
                .height(200.dp)
                .width(140.dp)
        )

        //Espacio
        Spacer(modifier = Modifier.height(50.dp))

        //Si hay error aparece el texto en color rojo error
        if(mensajeError != "") {
            Text(text = loginEstado!!.LoginError,
                color = MaterialTheme.colors.error,)
        }

        if (loginEstado?.cargando == true ) {
            CircularProgressIndicator()

        }

        Spacer(modifier = Modifier.height(20.dp))


        TextField(
            value = loginEstado?.nombre ?: "",
            onValueChange = { viewModel?.cambionombre(it)},
            label = { Text("Email") },
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.White),
            leadingIcon = {
                Icon(Icons.Outlined.Mail, contentDescription = "")
            },
            isError = mensajeError != "",
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Email, imeAction = ImeAction.Done
            ),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = MaterialTheme.colors.primary,
                unfocusedIndicatorColor = MaterialTheme.colors.primary
            )


            )

        Spacer(modifier = Modifier.height(16.dp))

        TextField(value = loginEstado?.contra ?: "",
            onValueChange = { viewModel?.cambioContra(it)},
            label = { Text("Contraseña") },
            visualTransformation = PasswordVisualTransformation(),
            modifier = Modifier
                .fillMaxWidth()
                .background(Color.White),
            leadingIcon = {
                Icon(Icons.Outlined.Key, contentDescription = "")
            },
            isError = mensajeError != "",
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(onDone = {
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager})
                ,colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = MaterialTheme.colors.primary,
                unfocusedIndicatorColor = MaterialTheme.colors.primary
            )       )


        Spacer(modifier = Modifier.height(16.dp))

        //Boton iniciar sesion
        Button(shape = CircleShape,
            onClick = { viewModel?.iniciarSesion(context) } ,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text("Iniciar sesión")
        }


        ClickableText(
            text = buildAnnotatedString {
                withStyle(
                    style = SpanStyle(color = Color.Black, fontWeight = FontWeight.Bold)
                ){
                    append("¿No tienes cuenta?")
                }

                withStyle(
                    style = SpanStyle(color = Color(0xFF2DB4CC), fontWeight = FontWeight.Bold)
                ){
                    append(" Crear cuenta")
                }

            }, onClick = {
                navController.navigate(Destination.PantallaRegistro.ruta)
            }
        )



        Spacer(modifier = Modifier.height(56.dp))


    }

    }


@Composable
fun PantallaRegistro(viewModel: LoginViewModel?, navController: NavController) {

    //vaiables
    val loginEstado = viewModel?.loginEstado
    val mensajeError = loginEstado?.RegistroError //en esta variable en el viewmodel vamos cambiando su valor , si sigue vacia no hemos cambiado valor , no hay errores
    val context = LocalContext.current


    //Acciones pantalla
    LaunchedEffect(key1 = viewModel?.usuarioIniciado){
        if (viewModel?.usuarioIniciado == true){
            navController.navigate(Destination.PantallaPerfil.ruta)
        }
    }

    //Dialogo como registrarse
    var verDialogoRegistro by rememberSaveable { mutableStateOf(true) }
    DialogoRegistro(ver = verDialogoRegistro, aceptar = {verDialogoRegistro = false})




    //Empieza el orden de componentes
    Column(
        modifier = Modifier.fillMaxSize().padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center

    ) {

        //Cabecera
        Text(
            text = "Registrarse",
            style = MaterialTheme.typography.h3,
            fontWeight = FontWeight.Black,
            color = MaterialTheme.colors.primary
        )


        Spacer(modifier = Modifier.height(15.dp))

        //Imagen
        Image(painter = painterResource(R.drawable.logo),
            contentDescription ="",
            modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .fillMaxWidth()
                        .height(200.dp)
                        .width(140.dp)
        )


        Spacer(modifier = Modifier.height(50.dp))


        if(mensajeError != "") {
            Text(text = loginEstado!!.RegistroError,
                color = Color.Red,)
        }

        if (loginEstado?.cargando == true ) {
            CircularProgressIndicator()
        }

        Spacer(modifier = Modifier.height(20.dp))


        TextField(
            value = loginEstado?.nombreRegistro ?: "",
            onValueChange = { viewModel?.cambionombreRegistro(it)},
            label = { Text("Email") },
            modifier = Modifier.fillMaxWidth().background(Color.White),
            leadingIcon = {
                Icon(Icons.Outlined.Mail, contentDescription = null)
            },
            isError = mensajeError!= "",
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Email, imeAction = ImeAction.Done
            ),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = MaterialTheme.colors.primary,
                unfocusedIndicatorColor = MaterialTheme.colors.primary
            )

        )

        Spacer(modifier = Modifier.height(16.dp))


        TextField(value = loginEstado?.contraRegistro ?: "",
            onValueChange = { viewModel?.cambiocontraRegistro(it)},
            label = { Text("Contraseña") },
            visualTransformation = PasswordVisualTransformation(),
            modifier = Modifier.fillMaxWidth().background(Color.White),
            leadingIcon = {
                Icon(Icons.Outlined.Key, contentDescription = null)
            },
            isError = mensajeError!= "",
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(onDone = {
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager}),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = MaterialTheme.colors.primary,
                unfocusedIndicatorColor = MaterialTheme.colors.primary
            )
        )
        Spacer(modifier = Modifier.height(16.dp))


        TextField(value = loginEstado?.confirmarContra ?: "",
            onValueChange = { viewModel?.cambiocontraConfirm(it)},
            label = { Text("Confirmar Contraseña") },
            visualTransformation = PasswordVisualTransformation(),
            modifier = Modifier.fillMaxWidth().background(Color.White),
            leadingIcon = {
                Icon(Icons.Outlined.Key, contentDescription = null)
            },
            isError = mensajeError!= "",
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
            keyboardActions = KeyboardActions(onDone = {
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager}),
            colors = TextFieldDefaults.textFieldColors(
                backgroundColor = Color.Transparent,
                focusedIndicatorColor = MaterialTheme.colors.primary,
                unfocusedIndicatorColor = MaterialTheme.colors.primary
            )
        )

        Spacer(modifier = Modifier.height(16.dp))

        Button(shape = CircleShape,
            onClick = { viewModel?.crearUsuario(context) },
            modifier = Modifier.fillMaxWidth()
        ) {
            Text("Registrarme")
        }


        ClickableText(
            text = buildAnnotatedString {
                withStyle(style = SpanStyle(color = Color.Black,fontWeight = FontWeight.Bold)
                ){
                    append("¿Ya tienes cuenta?")
                }

                withStyle(style = SpanStyle(color = Color(0xFF2DB4CC),fontWeight = FontWeight.Bold)
                ){
                    append(" Iniciar sesión")
                }

            }, onClick = { navController.navigate(Destination.PantallaLogin.ruta) }
        )


        Spacer(modifier = Modifier.height(56.dp))


    }

}


@Composable
fun DialogoRegistro(ver: Boolean,aceptar: () -> Unit){
    if (ver){
        AlertDialog(onDismissRequest = {} ,
            confirmButton = {
                TextButton(onClick = { aceptar()}) {
                    Text(text = "Aceptar")
                }
            },
            text = {Text(text = "Para registrarse deberás añadir un email válido y una contraseña no menor a 6 carácteres.\n\nAl registrarse permitirá que otros usuarios de la app puedan interactuar contigo.")},
            title = { Text(text = "¡Bienvenid@ a GymDiary!")}
        )
    }
}










