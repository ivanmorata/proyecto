package com.example.navigation.pantallas.Notas.ver

import android.widget.Toast
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.NoteAdd
import androidx.compose.material.icons.filled.Send
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.navigation.Repositorios.ConexionBBDD.Notas.Notas
import com.example.navigation.Repositorios.ConexionBBDD.Notas.ResultadoNotas
import com.example.navigation.navegacion.Destination
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PantallaVerNotas( viewModel: VerNotasViewModel?, navController: NavController,
 clickNota:(id:String) -> Unit){


    //Variables
        val notaEstado = viewModel?.notaEstado ?: VerNotasEstado()
        var notaSeleccionado: Notas? by remember { mutableStateOf(null) }
        //Aviso mensaje
        val context = LocalContext.current
         val nombreConEmail = viewModel?.nombre
         val nombreSinEmail = viewModel?.nombre.toString().substringBefore('@') //variable que usamos para el mensaje de envio, enviada por..


    // LaunchedEffects al entrar a la pantalla
        LaunchedEffect(key1 = viewModel?.usuarioIniciado) {
            if (viewModel?.usuarioIniciado == false) {
                navController.navigate(Destination.PantallaLogin.ruta)
            }
        }


        LaunchedEffect(key1 = Unit) {
            viewModel?.cargarNotas()
        }

        var emailTextt by remember { mutableStateOf("") }
        var idUsuarioEnvio by remember { mutableStateOf("") }


        LaunchedEffect(emailTextt) {
            val UsuarioEnvio = viewModel?.obtenerIdUsuarioPorEmail(emailTextt)
            idUsuarioEnvio = UsuarioEnvio ?: ""

        }

        //Dialogo eliminar nota
        var verDeleteNota by rememberSaveable { mutableStateOf(false) }

        //Dialogo enviar Nota
        var verEnviarEmail by rememberSaveable { mutableStateOf(false) }

        //Dialgo confirmar enviar nota
        var verConfirmacionEmail by rememberSaveable { mutableStateOf(false) }

    //Dialogo nota vacia
    var verNotaVacia by rememberSaveable { mutableStateOf(false) }
    DialogoNotasVacios(verNotaVacia, aceptar = { navController.navigate(Destination.PantallaEditNotas.ruta) }, cancelar = { navController.navigate(Destination.PantallaPerfil.ruta) } )

    //Dialogo email erroneo
    var mensajeEmailErroneo by remember { mutableStateOf("") }

    var verEmailErroneo by rememberSaveable { mutableStateOf(false) }
    DialogoEmailerroneo(ver = verEmailErroneo, mensaje = mensajeEmailErroneo, aceptar = {verEmailErroneo = false})




    //Parte superior pantalla + estructura
        Scaffold(topBar = {
            TopAppBar() {

                Spacer(modifier = Modifier.width(8.dp))
                Text(text = "Notas", fontSize = 18.sp)
                Spacer(modifier = Modifier.width(290.dp))
                IconButton(onClick = { navController.navigate(Destination.PantallaEditNotas.ruta) }) {
                    Icon(imageVector = Icons.Default.NoteAdd, contentDescription = null,)
                }
            }
        }

        ) { padding ->
            Column(modifier = Modifier.padding(padding)) {
                when (notaEstado.listaNotas) {
                    is ResultadoNotas.Cargando -> {
                        CircularProgressIndicator(
                            modifier = Modifier
                                .fillMaxSize()
                                .wrapContentSize(align = Alignment.Center)
                        )
                    }


                    is ResultadoNotas.Correcto -> {
                        LazyVerticalGrid(
                            cells = GridCells.Fixed(2),
                            contentPadding = PaddingValues(16.dp),
                            modifier = Modifier.padding(bottom = 50.dp),
                        ) {

                            if (notaEstado.listaNotas.info!!.isEmpty()){
                                verNotaVacia = true
                            }
                            items(notaEstado.listaNotas.info ?: emptyList()) { nota ->



                                LaunchedEffect(emailTextt) {
                                    val UsuarioEnvio =
                                        viewModel?.obtenerIdUsuarioPorEmail(emailTextt)
                                    idUsuarioEnvio = UsuarioEnvio ?: ""
                                try {
                                     if (emailTextt != null && emailTextt.isNotEmpty()) {
                                             if (viewModel?.emailExiste(emailTextt) == false) {
                                              verConfirmacionEmail = false
                                                 verEmailErroneo = true
                                                 mensajeEmailErroneo = "El usuario al que intenta enviar la nota no se ha registrado en GymDiary."
                                                  }
                                                 }
                                            }catch (e:FirebaseAuthInvalidCredentialsException){
                                    verConfirmacionEmail = false
                                    verEmailErroneo = true
                                    mensajeEmailErroneo = "El formato del email al que intenta enviar la nota es incorrecto."



                                }

                                }
                                //llamamos aqui a la funcion del Alert para coger el idnota
                                DialogoEliminarNota(ver = verDeleteNota,
                                    aceptar = {
                                        viewModel?.deleteNota(notaSeleccionado!!.idNota)
                                        verDeleteNota = false
                                        Toast.makeText(
                                            context,
                                            "Nota eliminada",
                                            Toast.LENGTH_SHORT
                                        ).show()

                                    },
                                    cancelar = { navController.navigate(Destination.PantallaVerNotas.ruta) })

                                DialogoenviarNota(ver = verEnviarEmail,
                                    aceptar = { emailText ->
                                        emailTextt = emailText
                                        verEnviarEmail = false
                                        verConfirmacionEmail = true

                                    },
                                    cancelar = { navController.navigate(Destination.PantallaVerNotas.ruta) }
                                )

                                DialogoConfirmarEnvio(
                                    ver = verConfirmacionEmail, aceptar = {

                                        if(emailTextt.isEmpty()) {
                                            verEmailErroneo = true
                                            mensajeEmailErroneo = "El email no puede estar vacio."
                                            verConfirmacionEmail = false

                                        } else if (emailTextt == nombreConEmail) {
                                                verEmailErroneo = true
                                                mensajeEmailErroneo =
                                                    "No puedes enviar una nota a ti mismo."
                                                verConfirmacionEmail = false
                                            }

                                        else {
                                            viewModel?.EnviarNota(
                                                idEnvio = idUsuarioEnvio,
                                                infoNota = "**Nota enviada por ${nombreSinEmail}**\n\n ${notaSeleccionado!!.infoNota}",
                                                titulo = notaSeleccionado!!.titulo,
                                                color = notaSeleccionado!!.color,
                                                imagen = notaSeleccionado!!.imagenUrl,
                                                video = notaSeleccionado!!.videoUrl
                                            )
                                            Toast.makeText(
                                                context,
                                                "¡Nota enviada!",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                            verConfirmacionEmail = false

                                        }
                                    },
                                    cancelar = { verConfirmacionEmail = false },
                                    email = emailTextt
                                )

                                        NotaItem(notas = nota,
                                            compartirClick = {
                                                verEnviarEmail = true
                                                notaSeleccionado = nota


                                            }, borrarClick = {
                                                notaSeleccionado = nota
                                                verDeleteNota = true
                                            },
                                            click = { clickNota(nota.idNota) }
                                        )
                            }
                        }
                    }
                    else -> {
                        Text(
                            text = notaEstado.listaNotas.throwable?.localizedMessage
                                ?: "Error desconocido"
                        )

                    }

                }

            }
        }

    }


@OptIn(ExperimentalFoundationApi::class)
@Composable
fun NotaItem(notas:Notas,
             click:()-> Unit,compartirClick:()->Unit,
             borrarClick:()->Unit){

val context = LocalContext.current

    Card(
        modifier = Modifier
            .padding(8.dp)
            .combinedClickable(onClick = { click() })
            .fillMaxSize(),
        backgroundColor = listaColores.colores[notas.color],
        shape = RoundedCornerShape(17.dp)
    ) {

        Column {
            Text(
                text = notas.titulo,
                style = MaterialTheme.typography.h6,
                fontWeight = FontWeight.Bold,
                maxLines = 1,
                overflow = TextOverflow.Clip,
                modifier = Modifier.padding(4.dp)
            )
            Spacer(modifier = Modifier.size(4.dp))

            CompositionLocalProvider(
                LocalContentAlpha provides ContentAlpha.disabled
            )
            {
                Text(
                    text = notas.infoNota, style = MaterialTheme.typography.body1,
                    overflow = TextOverflow.Ellipsis, modifier = Modifier.padding(4.dp),
                    maxLines = 4
                )
            }

            Spacer(modifier = Modifier.size(4.dp))

            CompositionLocalProvider(
                LocalContentAlpha provides ContentAlpha.disabled
            )
            {
                Text(
                    text = "", style = MaterialTheme.typography.body1,
                    overflow = TextOverflow.Ellipsis, modifier = Modifier
                        .padding(4.dp)
                        .align(Alignment.End),
                    maxLines = 4
                )
            }

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 4.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                IconButton(onClick = { borrarClick() }) {
                    Icon(Icons.Default.Delete, contentDescription = "Enviar")
                }
                IconButton(onClick = { compartirClick() }) {
                    Icon(Icons.Default.Send, contentDescription = "BorrarNota")
                }
            }

        }


    }

    }

@Composable
fun DialogoNotasVacios(ver: Boolean,aceptar: () -> Unit,cancelar: () -> Unit){
    if (ver){
        AlertDialog(onDismissRequest = {},
            confirmButton ={
                TextButton(onClick = { aceptar() }) {
                    Text(text = "Empezar")
                }
            },
            dismissButton = {
                TextButton(onClick = { cancelar() }) {
                    Text(text = "Volver a inicio")
                }
            },
            title = { Text(text = "No hay ninguna nota guardada") },
            text = { Text(text = "Empieza a guardar notas sobre tu vida fitness, podrás añadir imágenes o vídeos de tu galería y compartirlas con otros usuarios de la app.") },

            )
    }
}


@Composable
fun DialogoEmailerroneo(ver: Boolean,mensaje:String,aceptar: () -> Unit){
    if (ver){
        AlertDialog(onDismissRequest = {}, confirmButton = {
            Button(
                onClick = { aceptar() },
            ) {
                Text(text = "Aceptar")
            }
        }, title ={ Text(text = "Error al enviar")},
            text = { Text(text = mensaje)}
        )
    }
}
@Composable
fun DialogoConfirmarEnvio(ver:Boolean,aceptar: () -> Unit,cancelar: () -> Unit,email:String) {
    if (ver) {
        AlertDialog(
            onDismissRequest = { cancelar() },
            confirmButton = {

                Button(
                    onClick = { aceptar() },
                ) {
                    Text(text = "Enviar")
                }
            },
            dismissButton = {
                Button(
                    onClick = { cancelar() },
                ) {
                    Text(text = "Cancelar")
                }
            },
            text = {
                Text(text = "¿Estás seguro que quieres enviar tu nota a ${email}?")
            }


        )

    }
}





    @Composable
    fun DialogoenviarNota(ver: Boolean, aceptar: (String) -> Unit, cancelar: () -> Unit) {

        if (ver) {
            var emailText by remember { mutableStateOf("") }

            AlertDialog(
                onDismissRequest = { cancelar() },
                confirmButton = {
                    Button(
                        onClick = { aceptar(emailText) },
                    ) {
                        Text(text = "Aceptar")
                    }
                },
                dismissButton = {
                    Button(
                        onClick = { cancelar() },
                    ) {
                        Text(text = "Cancelar")
                    }
                }, text = {

                    Column {
                        Text(text = "Usuario al que quieras enviar la nota")

                        Spacer(modifier = Modifier.height(25.dp)) // Agrega un espacio horizontal de 8dp

                        TextField(
                            value = emailText,
                            onValueChange = { emailText = it },

                            label = { Text("Email")
                                    },
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color.Transparent,
                                focusedIndicatorColor = MaterialTheme.colors.primary,
                                unfocusedIndicatorColor = MaterialTheme.colors.primary
                            ),
                        )
                    }


                }, title = { Text(text = "Enviar nota",
                style = TextStyle(fontSize = 22.sp, fontWeight = FontWeight.Bold)
                )}


            )
        }
    }


    @Composable
    fun DialogoEliminarNota(ver: Boolean, aceptar: () -> Unit, cancelar: () -> Unit) {
        if (ver) {
            AlertDialog(
                onDismissRequest = { cancelar() },
                confirmButton = {
                    TextButton(onClick = { aceptar() }) {
                        Text(text = "Aceptar")
                    }
                },
                dismissButton = {
                    TextButton(onClick = { cancelar() }) {
                        Text(text = "Cancelar")
                    }
                },
                title = { Text(text = "Eliminar nota") },
                text = { Text(text = "¿Desea eliminar esta Nota?") },

                )
        }
    }
