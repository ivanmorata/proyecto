package com.example.navigation.pantallas.Splash

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.example.navigation.navegacion.Destination
import kotlinx.coroutines.delay
import com.example.navigation.R


@Composable
fun PantallaSplashScreen(navController: NavController){

    LaunchedEffect(key1 = true){
        delay(2700)
        navController.popBackStack() //no podremos volver a la pantalla
        navController.navigate(Destination.PantallaLogin.ruta) //pasados 4 segundos viajamos al login
    }

    Splash(navController)
}


@Composable
fun Splash(navController: NavController){
Column(
    modifier = Modifier.fillMaxSize(),
    verticalArrangement = Arrangement.Center,
    horizontalAlignment = androidx.compose.ui.Alignment.CenterHorizontally

) {
    Image(painter = painterResource(id =R.drawable.logo),
        contentDescription = "", modifier = Modifier.height(290.dp)
            .width(230.dp)
    )

    

}
}
