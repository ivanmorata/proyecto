package com.example.navigation.pantallas.Notas.ver

import com.example.navigation.Repositorios.ConexionBBDD.Notas.Notas
import com.example.navigation.Repositorios.ConexionBBDD.Notas.ResultadoNotas


data class VerNotasEstado(
    val listaNotas: ResultadoNotas<List<Notas>> = ResultadoNotas.Cargando(),

    val borrarNotaEstado:Boolean = false
)
