package com.example.navigation.pantallas.Notas

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.navigation.NavController
import coil.compose.rememberImagePainter
import com.example.navigation.navegacion.Destination
import com.example.navigation.pantallas.Notas.ver.listaColores
import com.example.navigation.pantallas.Notas.addEdit.EditNotasViewModel
import com.example.navigation.pantallas.Notas.addEdit.EditarNotasEstado
import com.example.navigation.pantallas.VerEjercicios.DialogoCamposVacios
import com.example.navigation.R
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util




@SuppressLint("CoroutineCreationDuringComposition")
@Composable
fun PantallaEditNotas(navController: NavController,
                      viewModel: EditNotasViewModel?, idNota:String
               ){

    var imagenUri by remember { mutableStateOf<Uri?>(null) }
    var videoUri by remember { mutableStateOf<Uri?>(null) }

    val context = LocalContext.current

    val abrirGaleriaImagen = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent()){ uri: Uri? ->
        if(uri != null){
            videoUri = uri
            viewModel?.cambioVideoUri(uri)
                    }
        }


    val abrirGaleriaVideo = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.GetContent()){ uri: Uri? ->
        if(uri != null){
            imagenUri = uri
            viewModel?.cambioImagenUri(uri)
        }
    }

    val notaEstado = viewModel?.notaEstado ?: EditarNotasEstado()




    //el icono cambiara en la pantalla de añadir/editar dependiendo si obtiene un idNota ya creado anteriormente
    val icono = if (!idNota.isNotBlank()) {
        Icons.Default.Save //nueva nota
    } else {
        Icons.Default.Update //actualizamos nota
    }




    LaunchedEffect(key1 = Unit){
        if (idNota.isNotBlank()){
            viewModel?.obtenerNota(idNota)
        }else  {
            viewModel?.TextNotasReset() //???estado resett?
        }
    }

    val elegirColor by animateColorAsState(targetValue = listaColores.colores[notaEstado.color])

    //Dialogo campos vacios
    var verDialogoCamposVacios by rememberSaveable { mutableStateOf(false) }
    DialogoCamposVacios(verDialogoCamposVacios,{verDialogoCamposVacios = false})


    var verProgressBar by rememberSaveable { mutableStateOf(false) }

    val ImagenNoVacia = notaEstado.imagenUri != null && notaEstado.imagenUri.toString().isNotEmpty()
    val VideoNoVacio = notaEstado.videoUri != null && notaEstado.videoUri.toString().isNotEmpty()


    Scaffold(
        floatingActionButton = {
                FloatingActionButton(
                    onClick = {
                        if (idNota.isNotBlank()) {
                            if (notaEstado.infoNota.isEmpty() or notaEstado.titulo.isEmpty()){
                                verDialogoCamposVacios = true
                            }else{
                                viewModel?.updateNota(idNota)
                            }
                        }
                        else{
                            if (notaEstado.infoNota.isEmpty() or notaEstado.titulo.isEmpty()){
                                verDialogoCamposVacios = true
                            } else{
                            viewModel?.insertNota()
                                verProgressBar = true

                            }
                        }
                    },
                    backgroundColor = Color(0xFF2DB4CC),
                    modifier = Modifier.padding(bottom = 55.dp)
                ) {
                    Icon(imageVector = icono , contentDescription = null )
                }

        },
    ) {


        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(color = elegirColor)
                .verticalScroll(rememberScrollState())


        ) {


            LazyRow(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceEvenly,
                contentPadding = PaddingValues(vertical = 16.dp, horizontal = 8.dp,)
            )

            {
                itemsIndexed(listaColores.colores) { elegirColor, color ->
                    ColorItem(color = color) {
                        viewModel?.cambioColor(elegirColor)
                    }
                }

            }

            OutlinedTextField(
                value = notaEstado.titulo,
                onValueChange = {
                    if (it.length <= 30){
                        viewModel?.cambioTitulo(it)}},
                label = { Text(text = "Nombre nota") },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(10.dp)
            )

            OutlinedTextField(
                value = notaEstado.infoNota,
                onValueChange = {  if (it.length <= 300){
                    viewModel?.cambioinfoNota(it) }},
                label = { Text(text = "Descripción") },
                modifier = Modifier
                    .fillMaxSize()
                    .padding(10.dp),
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(onDone = {
                    context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager})
            )

            if (!idNota.isNotBlank() && !ImagenNoVacia) {
                Row(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(horizontal = 16.dp)
                        .padding(bottom = 16.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {


                    Button(
                        onClick = { abrirGaleriaImagen.launch("image/*") },
                        modifier = Modifier.weight(1f)
                            .padding(end = 8.dp), shape = CircleShape
                        )
                    {
                        Icon(Icons.Default.AddPhotoAlternate, contentDescription = "Agregar foto")
                    }

                    Button(
                        onClick = { abrirGaleriaVideo.launch("video/*") },
                        modifier = Modifier.weight(1f)
                            .padding(start = 8.dp), shape = CircleShape
                        )
                    {
                        Icon(Icons.Default.PlayCircle, contentDescription = "Agregar Video")
                    }
                }
            }

            if (verProgressBar == true){
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .wrapContentSize(Alignment.Center)
                ) {
                    CircularProgressIndicator()
                }
            }


            if (ImagenNoVacia) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth(1f)
                        .aspectRatio(1f)
                ) {
                    verVideoExoPlayer(notaEstado.imagenUri.toString())
                }


            } else if (VideoNoVacio){
                Image(
                    painter = rememberImagePainter(notaEstado.videoUri),
                    contentDescription = "",
                    modifier = Modifier
                        .fillMaxWidth()
                        .aspectRatio(1f)
                )
            }

            else{

                Image(painter = rememberImagePainter(R.drawable.notavacia),
                   contentDescription = "",
                    modifier = Modifier
                        .fillMaxWidth()
                        .aspectRatio(1f)
                )
            }





            if (notaEstado.estadoNotaNueva) {
                    Toast.makeText(context,"¡Nueva nota añadida!", Toast.LENGTH_SHORT).show()
                    viewModel?.TextNotasReset()
                    navController.navigate(Destination.PantallaVerNotas.ruta) {
                        popUpTo(Destination.PantallaEditNotas.ruta) {inclusive = true}
                    }

            }

            if (notaEstado.estadoNotaActualz) {
                    Toast.makeText(context,"¡Nota actualizada!", Toast.LENGTH_SHORT).show()
                    viewModel?.TextNotasReset()
                    navController.navigate(Destination.PantallaVerNotas.ruta)
                }

            Spacer(modifier = Modifier.height(56.dp))

        }

    }


}

fun isImagen(url: String): Boolean {
    val extension = MimeTypeMap.getFileExtensionFromUrl(url)
    val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension)
    return mimeType?.startsWith("image") == true
}

@Composable
fun verVideoExoPlayer(videoUrl: String){

    val context = LocalContext.current
    val exoPlayer = remember {
        SimpleExoPlayer.Builder(context).build().apply {
            val dataSourceFactory : com.google.android.exoplayer2.upstream.DataSource.Factory = DefaultDataSourceFactory(context,
            Util.getUserAgent(context, context.packageName))

            val source = ProgressiveMediaSource.Factory(dataSourceFactory)
                .createMediaSource((Uri.parse(videoUrl)))
            this.prepare(source)
        }
    }

    AndroidView(
        factory = { context ->
            PlayerView(context).apply {
                player = exoPlayer
            }
        },
        modifier = Modifier.fillMaxSize()
    )

    DisposableEffect(Unit) {
        onDispose {
            exoPlayer.release()
        }
    }
}


@Composable
fun ColorItem( color : androidx.compose.ui.graphics.Color, click:() -> Unit){
    Surface(color = color, shape = CircleShape, modifier = Modifier
        .padding(8.dp)
        .size(30.dp)
        .clickable {
            click.invoke()
        },
        border = BorderStroke(1.dp, androidx.compose.ui.graphics.Color.Black)
    ) {

    }
}





