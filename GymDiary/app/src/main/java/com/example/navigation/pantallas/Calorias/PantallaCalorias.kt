package com.example.navigation.pantallas

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.outlined.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.navigation.navegacion.Destination
import com.example.navigation.pantallas.Notas.ver.VerNotasViewModel
import com.example.navigation.R
import java.time.LocalDate
import java.time.format.DateTimeFormatter


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun PantallaCalorias(navController: NavController,viewModel: VerNotasViewModel?){

    //valores textfield y resultado del calculo
    var peso by remember { mutableStateOf("") }
    var altura by remember { mutableStateOf("") }
    var edad by remember { mutableStateOf("") }
    var resultadoMensaje by remember { mutableStateOf("") }
    var sexo by remember { mutableStateOf<Sex?>(null) }

    val idUsuario = viewModel?.idUsuario

    //lista de valores de clase actividad
    val nivelesActividad = listOf(
        actividad("Sedentario",1.25),
        actividad("Ligeramente activo",1.375),
        actividad("Activo",1.55),
        actividad("Muy activo",1.725)
    )

    var actividad by remember { mutableStateOf<actividad?>(null) }


    //lista de valores para la clase objetivo

    val nivelesobjetivo = listOf(
        objetivo("Ganancia de peso", 300, "ganar peso"),
        objetivo("Pérdida de peso", -400, "perder peso"),
        objetivo("Mantener el peso", 0,"mantener el peso")

        )
    var objetivo by remember { mutableStateOf<objetivo?>(null) }


    //variables para obtener fecha de hoy para agregar a notas con la fecha de hoy
    var fecha = ""
    val fechaHoy = LocalDate.now()
    val formato = DateTimeFormatter.ofPattern("dd/MM")
    fecha = fechaHoy.format(formato)


    //Dialogo incorrectas calorias
    var verErrorCalorias by rememberSaveable { mutableStateOf(false) }
    DialogoCaloriasIncorrectas(verErrorCalorias,
        mensaje= "Rellena correctamente los campos para hacer el cálculo de calorías.",
        Cerrar = { verErrorCalorias = false},
        titulomensaje = "Error"
    )

    //Dialogo envio calorias diarias a notas
    var verEnvioCalorias by rememberSaveable { mutableStateOf(false) }

    val context = LocalContext.current

    //Comprobacion si no hay usuario logeado que mande al login
    LaunchedEffect(key1 = viewModel?.usuarioIniciado) {
        if (viewModel?.usuarioIniciado == false) {
            navController.navigate(Destination.PantallaLogin.ruta)
        }
    }


    //Parte superior pantalla + estructura
    Scaffold (topBar = {
        TopAppBar(){
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Calorias", fontSize = 18.sp)
        }
    }

    ){
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
                .verticalScroll(rememberScrollState()),
            horizontalAlignment = Alignment.CenterHorizontally,
        )  {

            Text("Calculadora calorías diarias",
                style = TextStyle(
                    color = MaterialTheme.colors.primary,
                    fontSize = 26.sp,
                    fontWeight = FontWeight.Black,
                ),
                modifier = Modifier.align(Alignment.CenterHorizontally)

            )

            Spacer(modifier = Modifier.height(16.dp))


            Image(painter = painterResource(R.drawable.fotocalculadoracalorias),
                contentDescription = "",
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .fillMaxWidth()
            )

            Spacer(modifier = Modifier.height(16.dp))


            TextField(value = peso,
                onValueChange = {
                    if (it.length <= 6) {
                        peso = it }},
                label = { Text("Peso") },
                modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.White),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                leadingIcon = {
                    Icon(Icons.Outlined.MonitorWeight, contentDescription = null)
                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    focusedIndicatorColor = MaterialTheme.colors.primary,
                    unfocusedIndicatorColor = MaterialTheme.colors.primary
                )

            )

            Spacer(modifier = Modifier.height(16.dp))


            TextField(value = altura,
                onValueChange = {
                    if (it.length <= 6) {
                    altura = it }},
                label = { Text("Altura en cm") },
                modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.White),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                leadingIcon = {
                    Icon(Icons.Outlined.Height, contentDescription = "")
                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    focusedIndicatorColor = MaterialTheme.colors.primary,
                    unfocusedIndicatorColor = MaterialTheme.colors.primary
                ))


            TextField(value = edad,
                onValueChange = {
                    if (it.length <= 3) {
                    edad = it }},
                label = { Text("Edad en años") },
                modifier = Modifier
                    .fillMaxWidth()
                    .background(Color.White),
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                leadingIcon = {
                    Icon(Icons.Outlined.Pin, contentDescription = "")
                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    focusedIndicatorColor = MaterialTheme.colors.primary,
                    unfocusedIndicatorColor = MaterialTheme.colors.primary
                ))

            Spacer(modifier = Modifier.height(22.dp))


            listaDropDownSex(sexo) {
                sexo = it
            }

            listaDropDownActividad( nivelesActividad, actividad ,{
                actividad = it
            })

            listaDropDownObjetivo(nivelesobjetivo , objetivo,{
                objetivo = it
            })

            //Llamada a funcion DialogoEnvioCalorias, enviamos calorias a notas
            DialogoEnvioCalorias(ver = verEnvioCalorias,
                aceptar = {
                    if (idUsuario != null) {
                        viewModel.EnviarNota(
                            idEnvio = idUsuario,
                            imagen = "",
                            titulo = "Calorías ${fecha}",
                            infoNota = "Mis calorías objetivo para ${objetivo?.mensajeObjetivo ?: ""}: " +
                                    " ${resultadoMensaje}\n\nCalorías desayuno:\n\nCalorías comida:\n\n" +
                                    "Calorías merienda:\n\nCalorías cena: ",
                            color = 1,
                            video = "https://firebasestorage.googleapis.com/v0/b/saluddeporte-d6bad.appspot.com/o/imagenesNotas%2FfotoCalculadoraCalorias.png?alt=media&token=487f16af-6189-4708-a3ce-66f200387e4a&_gl=1*7cvtvq*_ga*OTgxMTk2NTQuMTY4MzI3NjQ0NA..*_ga_CW55HF8NVT*MTY4NjY1MjgzNS43MC4xLjE2ODY2NzA3NDIuMC4wLjA."                        )

                        verEnvioCalorias = false
                        navController.navigate(Destination.PantallaVerNotas.ruta)
                    }
                },
                cancelar = { verEnvioCalorias=false },
                calorias = resultadoMensaje,
                objetivo = objetivo?.mensajeObjetivo ?: ""
                )


            Spacer(modifier = Modifier.height(15.dp))


            Button(shape = CircleShape,
                onClick = {
                    try {
                        val calorias = calcularCalorias(
                            peso.toDouble(),
                            altura.toDouble(),
                            edad.toInt(),
                            sexo!!,
                            actividad!!.valor,
                            objetivo!!.valor
                        )

                        resultadoMensaje = String.format("%.0f", calorias)
                        verEnvioCalorias = true

                    //si los campos estan mal agregados se hara visible la alerta de error
                    } catch (e: Exception) {
                        verErrorCalorias = true
                    }
                },

                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Calcular")
            }

            Spacer(modifier = Modifier.height(56.dp))

        }

    }
}


//Diferentes dropDownMenu: sexo,objetivo y nivel de actividad
@Composable
fun listaDropDownSex(valorSex: Sex?, ValorCambiado: (Sex?) -> Unit
) {
    var expandir by remember { mutableStateOf(false) }

    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(), value = valorSex?.toString() ?: "",
        onValueChange = {}, readOnly = true,
        label = { Text("Sexo") },
        trailingIcon = {
            IconButton(
                onClick = { expandir = true }) { Icon(Icons.Default.ArrowDropDown, contentDescription = "") }
        }
    )

    DropdownMenu(onDismissRequest = { expandir = false }, expanded = expandir)

    {
        DropdownMenuItem(onClick = { ValorCambiado(Sex.Hombre)
            expandir = false })

        { Text(text = "Hombre") }

        DropdownMenuItem(onClick = { ValorCambiado(Sex.Mujer)
            expandir = false })

        { Text(text = "Mujer") }
    }
}

@Composable
fun listaDropDownActividad( actividadLista: List<actividad>, nombreActividad: actividad?,
    ValorCambiado: (actividad?) -> Unit
) {
    var expandir by remember { mutableStateOf(false) }

    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        value = nombreActividad?.nombre ?: "",
        onValueChange = {},
        readOnly = true,
        label = { Text("Actividad física") },
        trailingIcon = {
            IconButton(onClick = { expandir = true }) {
                Icon(Icons.Default.ArrowDropDown, contentDescription = "")
            }
        }
    )

    DropdownMenu(
        expanded = expandir,
        onDismissRequest = { expandir = false }
    ) {
        actividadLista.forEach {  activ ->
            DropdownMenuItem(onClick = { ValorCambiado(activ)
                expandir = false })
            {
                Text(text = activ.nombre)
            }
        }
    }
}

@Composable
fun listaDropDownObjetivo(
    objetivoLista: List<objetivo>, nombreObjetivo: objetivo?,
    ValorCambiado: (objetivo?) -> Unit
) {
    var expandir by remember { mutableStateOf(false) }

    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        value = nombreObjetivo?.nombreObjetivo?: "",
        onValueChange = {},
        readOnly = true,
        label = { Text("Objetivo") },
        trailingIcon = {
            IconButton(onClick = { expandir = true }) {
                Icon(Icons.Default.ArrowDropDown, contentDescription = "")
            }
        }
    )

    DropdownMenu(
        expanded = expandir,
        onDismissRequest = { expandir = false }
    ) {
        objetivoLista.forEach { obj ->
            DropdownMenuItem(
                onClick = {
                    ValorCambiado(obj)
                    expandir = false
                }
            ) {
                Text(text = obj.nombreObjetivo)
            }
        }
    }
}

//Funcion que usaremos para obtener el numero de calorías
fun calcularCalorias(peso: Double, altura: Double, edad: Int, sexo:
Sex, actividad:Double, objetivo:Int): Double {
    // IMB es el indice metabolico basal
    val IMB = if (sexo == Sex.Hombre) {
        // las operaciones para este calculo se calculan diferentes en H y M
        66.47 + (13.75 * peso) + (5.0 * altura) - (6.75 * edad)
    } else {
        655.1 + (10 * peso) + (1.85 * altura) - (4.68 * edad)
    }
    return (IMB * actividad) + objetivo
}

enum class Sex{
    Hombre, Mujer
}

data class actividad(
    val nombre:String,
    val valor:Double
)

data class objetivo(
    val nombreObjetivo:String,
    val valor:Int,
    val mensajeObjetivo:String
)



@Composable
fun DialogoEnvioCalorias(ver: Boolean,aceptar: () -> Unit, cancelar:() ->Unit,calorias:String,objetivo:String){
    if (ver){
        AlertDialog(onDismissRequest = {cancelar()} ,
            confirmButton = {
                TextButton(onClick = { aceptar()}) {
                    Text(text = "Guardar")
                }
            },
            dismissButton = {
                TextButton(onClick = { cancelar()}) {
                    Text(text = "Cancelar")
                }
            },
            text = {Text(text = "Tus calorias diarias con el objetivo de ${objetivo} son ${calorias} calorias.")}
            )
    }
}


@Composable
fun DialogoCaloriasIncorrectas(ver:Boolean,titulomensaje:String,mensaje: String, Cerrar: () -> Unit) {
    if(ver) {
        AlertDialog(
            onDismissRequest = {Cerrar()},
            title = { Text(text = titulomensaje) },
            text = { Text(text = mensaje) },
            confirmButton = {
                Button(onClick = {Cerrar()}) {
                    Text(text = "Aceptar")
                }
            }
        )
    }
}












