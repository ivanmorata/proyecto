package com.example.navigation.Repositorios.ConexionBBDD.Notas

import android.net.Uri
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.UploadTask
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await


class RepositoyNotas() {


 //Recuperar infomacion de Firebase authentication
    fun usuario() = Firebase.auth.currentUser
    fun usuarioIiniciado(): Boolean= Firebase.auth.currentUser != null
    fun nombre (): String = Firebase.auth.currentUser?.email.toString()
    fun obtenerIdUsuario():String = usuario()?.uid.orEmpty()


//Funcion que nos devolvera el idUsuario si encuentra ese email en la colleccion usuarios sino devolvera nulo
    suspend fun obtenerIdUsuarioPorEmail(email: String): String? {
         try {
            val ListaUsuarios = FirebaseFirestore.getInstance().collection("usuarios")

            val consulta = ListaUsuarios.whereEqualTo("email", email).limit(1).get().await()
            if (!consulta.isEmpty) {
                val document = consulta.documents.first()
                return document.getString("idUsuario")
            } else {
                return null
            }
        } catch (e: Exception) {
           return null
        }
    }

    //funcion que nos devolvera verdadero falso si el email que escriba el usuario al enviar su nota esta registrado en Firebase Authentication o no no
    suspend fun emailExiste(email: String): Boolean {

        val emailEnvio = FirebaseAuth.getInstance().fetchSignInMethodsForEmail(email).await()

        return emailEnvio.signInMethods?.isNotEmpty() == true
    }



 // Coleccion donde vamos a guardar nuestras notas -> "Notas"
    private val ListaNotas : CollectionReference = Firebase.firestore.collection("Notas")


    fun obtenerNotasUsuario (idUsuario:String) : Flow<ResultadoNotas<List<Notas>>> = callbackFlow {

        var estadoEscucha:ListenerRegistration? = null

        try {
            estadoEscucha = ListaNotas.orderBy("creacion").whereEqualTo("idUsuario",idUsuario)
                .addSnapshotListener{ nota,e ->
                    val respuesta = if (nota != null){
                        val notas = nota.toObjects(Notas::class.java)
                        ResultadoNotas.Correcto(info = notas)
                    }else{
                        ResultadoNotas.Error(throwable = e?.cause)
                    }
                    trySend(respuesta)

                }
        }catch (e:java.lang.Exception){
            trySend(ResultadoNotas.Error(e?.cause))
            e.printStackTrace()
        }

        awaitClose {
            estadoEscucha?.remove()
        }
    }


    //a traves del idnota recuperaremos la nota para ver los datos para actualizarla
    fun obtenerNota(idNota : String, error: (Throwable?) -> Unit, Correct: (Notas?) -> Unit){
         ListaNotas.document(idNota).get()
             .addOnSuccessListener { correcto->
             Correct.invoke(correcto.toObject(Notas::class.java))
         }
             .addOnFailureListener{ fallo->
                 error.invoke(fallo.cause)
             }
    }


    //funciones insert, update y delete

    //Insert de nueva nota
    fun insertNota(idUsuario: String, titulo: String, infoNota: String, creacion: Timestamp,
                   color: Int = 0, imagenUri: Uri?, videoUri:Uri?, Completado: (Boolean) -> Unit){

        val idNota = ListaNotas.document().id
        val nota = Notas(idUsuario,titulo,infoNota,creacion, imagenUrl = "", videoUrl = "",
            color = color, idNota = idNota)

        if (imagenUri != null) {  //condicion para subir nota con imagen
            subirMultimedia(idNota, imagenUri) { imagenUrl ->
                if (imagenUrl != null) {
                    nota.imagenUrl = imagenUrl
                    ListaNotas.document(idNota).set(nota).addOnCompleteListener { insert ->
                        Completado(insert.isSuccessful)
                    }
                } else {
                    Completado(false)
                }
            }

        } else if (videoUri != null){  //condicion para subir nota con video
            subirMultimedia(idNota, videoUri) { videoUrl ->
                if (videoUrl != null) {
                    nota.videoUrl = videoUrl
                    ListaNotas.document(idNota).set(nota).addOnCompleteListener { insert ->
                        Completado(insert.isSuccessful)
                    }
                } else {
                    Completado(false)
                }
            }
        }

        else { // nota nueva sin imagen ni video
            ListaNotas.document(idNota).set(nota).addOnCompleteListener { insert ->
                Completado(insert.isSuccessful)
            }
        }
    }


    fun insertEnviarNota(idUsuario: String, titulo: String, infoNota: String, creacion: Timestamp,
        color: Int = 0, imagenUrl: String?, videoUrl:String?,
        Completado: (Boolean) -> Unit
    ) {
        val idNota = ListaNotas.document().id
        val nota = Notas(
            idUsuario, titulo, infoNota, creacion, imagenUrl = "",videoUrl = "",
            color = color, idNota = idNota
        )

        if (videoUrl != null) {
            nota.videoUrl = videoUrl
            ListaNotas.document(idNota).set(nota).addOnCompleteListener { insert ->
                Completado(insert.isSuccessful)
            }
        }

        if (imagenUrl != null){
            nota.imagenUrl = imagenUrl
            ListaNotas.document(idNota).set(nota).addOnCompleteListener{ insert ->
                Completado(insert.isSuccessful)
            }
        }

        else {
            ListaNotas.document(idNota).set(nota).addOnCompleteListener { insert ->
                Completado(insert.isSuccessful)
            }
        }
    }


    fun updateNota(titulo: String, infoNota:String, color: Int,
                   idNota: String, Completado: (Boolean) -> Unit){

        val actualizDatos = hashMapOf<String,Any>(
            "color" to color,
            "infoNota" to infoNota,
            "titulo" to titulo
        )
        ListaNotas.document(idNota).update(actualizDatos).addOnCompleteListener{ actualizado ->
            Completado(actualizado.isSuccessful)
        }
    }


    fun deleteNota(idNota: String,Completado: (Boolean) -> Unit){

        ListaNotas.document(idNota).delete().addOnCompleteListener{ borrado ->
            Completado.invoke(borrado.isSuccessful)
        }
    }

    fun cerrarSesion() = Firebase.auth.signOut()


    fun subirMultimedia(nombreArchivoMultimedia: String, archivo: Uri, pasamosUrl: (String?) -> Unit) {
        val ListaImagenesStorage = Firebase.storage.reference.
        child("imagenesNotas").child(nombreArchivoMultimedia)

        val cargarImagen = ListaImagenesStorage.putFile(archivo)

        cargarImagen.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { subida ->
            if (!subida.isSuccessful) {
                subida.exception?.let { throw it }
            }
            return@Continuation ListaImagenesStorage.downloadUrl
        }).addOnCompleteListener {
            if (it.isSuccessful) {
                val imagenUrl = it.result.toString()
                pasamosUrl(imagenUrl)
            } else {
                pasamosUrl(null)
            }
        }
    }







}