package com.example.navigation.navegacion

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.runtime.Composable
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.navigation.navegacion.Destination.*
import com.example.navigation.pantallas.*
import com.example.navigation.pantallas.Ejercicios.AddEjercicios.EjerciciosEditViewModel
import com.example.navigation.pantallas.Calorias.PantallaPeso
import com.example.navigation.pantallas.Calorias.PesoViewModel
import com.example.navigation.pantallas.Login.LoginViewModel
import com.example.navigation.pantallas.Notas.PantallaEditNotas
import com.example.navigation.pantallas.Notas.addEdit.EditNotasViewModel
import com.example.navigation.pantallas.Notas.ver.PantallaVerNotas
import com.example.navigation.pantallas.Notas.ver.VerNotasViewModel
import com.example.navigation.pantallas.Splash.PantallaSplashScreen
import com.example.navigation.pantallas.VerEjercicios.VerEjerciciosViewModel
import com.example.navigation.pantallas.VerEjercicios.editExercices
import com.example.navigation.pantallas.perfil.PantallaPerfil

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun Navegacion(
    navController: NavHostController
){
    NavHost(navController = navController , startDestination = PantallaSplaschScreen.ruta){

        /*Cada composable es el destino donde nos
         dirigiremos cuando usemos el navController para viajar entre pantallas */


            //Pantalla carga
            composable(PantallaSplaschScreen.ruta){
           PantallaSplashScreen(navController)
            }

            //login y registro
            composable(PantallaLogin.ruta){
                val viewModel : LoginViewModel = hiltViewModel()
               PantallaLogin(viewModel,navController)
            }

            composable(PantallaRegistro.ruta){
                val viewModel : LoginViewModel = hiltViewModel()
                PantallaRegistro(viewModel,navController)
            }



            //Peso y calorias
             composable(PantallaFormCalorias.ruta){
                 val viewModel : VerNotasViewModel = hiltViewModel()

                 PantallaCalorias(navController,viewModel)
             }

            composable(PantallaPeso.ruta){
            val viewModel : PesoViewModel = hiltViewModel()
            PantallaPeso(navController,viewModel)
         }



        //Perfil
            composable(PantallaPerfil.ruta){
                val viewModelEj: VerEjerciciosViewModel = hiltViewModel()

                val viewModel : VerNotasViewModel = hiltViewModel()
                PantallaPerfil(navController,viewModel,viewModelEj)
             }


        //Ejercicios
            composable(route = PantallaEjercicios.ruta){

                val viewModel: VerEjerciciosViewModel = hiltViewModel()
                val estado = viewModel.estado.value
                 val idUsuario:String

                PantallaEjercicios(
                    navController,
                    estado = viewModel.estado.value,
                        EjercicioClick = { id ->
                        navController.navigate(Destination.PantallaEditEjercicios.ruta + "?id=$id")
                    }, deleteEjercicio = viewModel::deleteEjercicio, viewModel = viewModel

                )
            }

            composable(route = PantallaEditEjercicios.ruta + "?id={id}") {
            val viewModel: EjerciciosEditViewModel = hiltViewModel()
            val estado = viewModel.estado.value

            editExercices(
                navController,
                estado = estado,
                insertEjercicio = viewModel::insertEjercicio,
                updateEjercicio = viewModel::updateEjercicio
            )
        }


        //Notas
        composable(PantallaVerNotas.ruta){
            val viewModel : VerNotasViewModel = hiltViewModel()
            PantallaVerNotas(
                viewModel = viewModel,
                navController = navController,
                clickNota = { idNota ->
                    navController.navigate(Destination.PantallaEditNotas.ruta + "?idNota=$idNota"){
                        launchSingleTop = true
                    }
                }
            )
        }

        composable(PantallaEditNotas.ruta + "?idNota={idNota}",
                arguments = listOf(
                    navArgument("idNota"){
                        type = NavType.StringType
                        defaultValue = ""
                    }
                ) ){ nota ->
                val viewModel : EditNotasViewModel = hiltViewModel()
            PantallaEditNotas(viewModel = viewModel,
                navController = navController,
                idNota = nota.arguments?.getString("idNota") as String )
            }


    }


}