package com.example.navigation.Repositorios.ConexionBBDD.Peso

sealed class ResultadoPeso<T> (
    val info: T? =  null,
    val mensaje: Throwable? = null
){
    class Correcto<T>(info: T?) : ResultadoPeso<T>(info = info)

    class Error<T>(throwable: Throwable?): ResultadoPeso<T>(mensaje = throwable)

    class Cargando<T>: ResultadoPeso<T>()
}