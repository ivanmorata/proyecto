package com.example.navigation.Repositorios.ConexionBBDD.Ejercicio

import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModulo {

    @Provides
    @Singleton
    fun obtenerBBDD() = FirebaseFirestore.getInstance()


    @Provides
    @Singleton
    fun obtenerTablaEjercicios(
        firestore: FirebaseFirestore
    ) = firestore.collection("Ejercicios")


}
