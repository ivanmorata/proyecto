package com.example.navigation.pantallas.Notas.ver

import androidx.compose.ui.graphics.Color

object listaColores {

    val colores = listOf(
        Color(0xFFffffff),
        Color(0xFFE991E4),
        Color(0xFF2DB4CC),
        Color(0xFF5EE194),
    )
}