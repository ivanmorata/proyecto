package com.example.navigation.pantallas.Calorias

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Context
import android.widget.DatePicker
import android.widget.Toast
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.navigation.Repositorios.ConexionBBDD.Notas.Peso
import com.example.navigation.Repositorios.ConexionBBDD.Peso.ResultadoPeso
import com.example.navigation.navegacion.Destination
import com.example.navigation.pantallas.VerEjercicios.DialogoCamposVacios
import java.util.*


@SuppressLint("CoroutineCreationDuringComposition")
@OptIn(ExperimentalFoundationApi::class)
@Composable
fun PantallaPeso(navController: NavController, viewModel: PesoViewModel?){

    //Variables
    val pesoEstado = viewModel?.pesoEstado ?: PesoEstado()
    val nuevoPesoEstado = viewModel?.nuevoPesoEstado ?: NuevoPesoEstado()
    var pesoSeleccionado: Peso? by remember { mutableStateOf(null) }


    //Mensaje de nuevo peso
    val context = LocalContext.current


    // LaunchedEffects al entrar a la pantalla
    LaunchedEffect(key1 = Unit){
        viewModel?.cargarPesos()
    }

    LaunchedEffect(key1 = viewModel?.usuarioIniciado){
        if(viewModel?.usuarioIniciado == false){
            navController.navigate(Destination.PantallaLogin.ruta)
        }
    }

    //variables boleanas para mostrar o ocultar los distintos dialogos
    var verListaVacia by rememberSaveable { mutableStateOf(false) }
    var verInsertPeso by rememberSaveable { mutableStateOf(false) }
    var verDeletePeso by rememberSaveable { mutableStateOf(false) }


    //Dialogo insertar peso
    DialogoNuevoPeso(verInsertPeso,
        aceptar = {
            viewModel?.insertPeso()
            verInsertPeso = false  //cambiamos el valor del booleano para al aceptar se oculte de nuevo el Dialogo de insercion
            verListaVacia = false
        },
        cancelar = { navController.navigate(Destination.PantallaPeso.ruta) }, viewModel)


    //Dialogo Eliminar peso
    /* ***** la llamada a la funcion en vez de hacerla aqui la haremos dentro del Row ,
     donde ahi si podemos coger el idPeso para el delete ****** */


    //Dialogo si pesos esta vacia
    DialogoPesosVacios(verListaVacia, {verInsertPeso = true
    verListaVacia = false} , {navController.navigate(Destination.PantallaPerfil.ruta)})


    //Parte superior pantalla + estructura
        Scaffold(
            topBar = {
                TopAppBar() {
                    Spacer(modifier = Modifier.width(8.dp))
                    Text(text = "Peso", fontSize = 18.sp)
                }

            },
            floatingActionButton = { //botones flotantes para nuevo peso y acceso a calculaora calorías
                Row(modifier = Modifier.padding(bottom = 50.dp)) {
                    FloatingActionButton(
                        onClick = { verInsertPeso = true },
                        backgroundColor = Color(0xFF2DB4CC),
                        contentColor = Color.White,
                        modifier = Modifier.padding(start = 4.dp)
                    ) {
                        Icon(imageVector = Icons.Default.Add, contentDescription = "Nuevo peso")
                    }

                    Spacer(modifier = Modifier.width(13.dp))

                    FloatingActionButton(
                        onClick = { navController.navigate(Destination.PantallaFormCalorias.ruta) },
                        backgroundColor = Color(0xFF0E4A8E),
                        contentColor = Color.White,
                        modifier = Modifier.padding(end = 10.dp)
                    ) {
                        Icon(
                            imageVector = Icons.Default.Calculate,
                            contentDescription = "Nuevo peso"
                        )
                    }
                }
            },
            floatingActionButtonPosition = FabPosition.Center,

            //Empieza el contenido de la pantalla
        ) { padding ->

            Column(
                modifier = Modifier.padding(15.dp)

            ) {

                when (pesoEstado.listaPeso) {
                    is ResultadoPeso.Cargando -> {
                        CircularProgressIndicator(
                            modifier = Modifier
                                .fillMaxSize()
                                .wrapContentSize(align = Alignment.Center)
                        )
                    }

                    is ResultadoPeso.Correcto -> {

                        LazyColumn(
                            contentPadding = PaddingValues(14.dp),
                            verticalArrangement = Arrangement.spacedBy(20.dp),
                            modifier = Modifier.padding(bottom = 50.dp)
                        ) {
                            if (pesoEstado.listaPeso.info!!.isEmpty() && verInsertPeso == false){
                                verListaVacia = true
                            }else {
                                verListaVacia = false
                                item {
                                    Text(
                                        text = "Seguimiento peso corporal",
                                        style = TextStyle(
                                            color = MaterialTheme.colors.primary,
                                            fontSize = 26.sp,
                                            fontWeight = FontWeight.Black
                                        )
                                    )
                                }

                            }

                            items(pesoEstado.listaPeso.info ?: emptyList()) { peso ->

                                DialogoEliminarPeso(verDeletePeso, aceptar = {
                                    viewModel?.deletePeso(pesoSeleccionado!!.idPeso)
                                    verDeletePeso = false
                                    Toast.makeText(context, "Peso eliminado", Toast.LENGTH_SHORT)
                                        .show()
                                }, cancelar = {
                                    verDeletePeso = false
                                })


                                Spacer(modifier = Modifier.height(16.dp))

                                Row(
                                    modifier = Modifier.fillMaxWidth(),
                                    verticalAlignment = Alignment.CenterVertically,
                                    horizontalArrangement = Arrangement.SpaceBetween
                                ) {


                                    Text(
                                        text = peso.peso + " kg",
                                        style = TextStyle(
                                            fontSize = 20.sp, fontWeight = FontWeight.Medium
                                        )
                                    )

                                    Spacer(modifier = Modifier.weight(1f))


                                    Text(
                                        text = peso.fecha,
                                        style = TextStyle(
                                            fontSize = 16.sp,
                                            fontWeight = FontWeight.Light,
                                            fontStyle = FontStyle.Italic
                                        )
                                    )

                                    //Icono de borrar
                                    IconButton(
                                        onClick = {
                                            verDeletePeso = true
                                            pesoSeleccionado = peso
                                        },
                                        modifier = Modifier.size(25.dp)
                                    ) {
                                        Icon(Icons.Filled.Delete, contentDescription = "")
                                    }
                                }


                            }

                        }



                        if (nuevoPesoEstado.estadoPesoNueva) {
                            Toast.makeText(context, "¡Nuevo peso registrado!", Toast.LENGTH_SHORT)
                                .show()
                            viewModel?.TextPesoReset()
                        }


                    }

                }


            }

        }


}

@Composable
fun DialogoPesosVacios(ver: Boolean,aceptar: () -> Unit,cancelar: () -> Unit){
    if (ver){
        AlertDialog(onDismissRequest = {},
        confirmButton ={
            TextButton(onClick = { aceptar() }) {
                Text(text = "Empezar")
            }
        },
            dismissButton = {
                TextButton(onClick = { cancelar() }) {
                    Text(text = "Volver a inicio")
                }
            },
            title = { Text(text = "Seguimiento de pesos vacío") },
            text = { Text(text = "Empieza a guardar diariamente el peso que diste en la bascula y seguir un control.\n\nEn la parte inferior de la pantalla está el acceso a la calculadora de calorías.") },

        )
    }
}


//Distintos de dialogos de eliminacion de peso, nuevo peso y mensaje de error de intentar guardar peso vacio
@Composable
fun DialogoEliminarPeso(ver:Boolean,aceptar: () -> Unit, cancelar: () -> Unit) {
    if (ver) {
        AlertDialog(
            onDismissRequest = { cancelar() },
            confirmButton = {
                TextButton(onClick = { aceptar() }) {
                    Text(text = "Aceptar")
                }
            },
            dismissButton = {
                TextButton(onClick = { cancelar() }) {
                    Text(text = "Cancelar")
                }
            },
            title = { Text(text = "Eliminar peso registrado") },
            text = { Text(text = "¿Desea eliminar este peso registrado?") },

            )
    }
}

@Composable
fun DialogoNuevoPeso(ver:Boolean,aceptar: () -> Unit,cancelar: () ->Unit,viewModel: PesoViewModel?) {

    var verVacio by rememberSaveable { mutableStateOf(false) }

    val nuevoPesoEstado = viewModel?.nuevoPesoEstado ?: NuevoPesoEstado()
    var fechaTexto by remember { mutableStateOf("") }

    DialogoCamposVacios(verVacio,{verVacio=false})
    if (ver) {
        AlertDialog(
            onDismissRequest = { cancelar() },
            confirmButton = {
                Button(
                    onClick = { if (nuevoPesoEstado.peso.isNotEmpty() && fechaTexto.isNotEmpty()){
                        aceptar()
                    }else{
                        verVacio = true

                    }
                              },
                    modifier = Modifier.padding(end = 8.dp)
                ) {
                    Text(text = "Aceptar")
                }
            },
            dismissButton = {
                Button(
                    onClick = { cancelar() },
                    modifier = Modifier.padding(start = 8.dp)
                ){
                    Text(text = "Cancelar")
                }
            },
            title = { Text(text = "Agregar nuevo peso" ,  style = TextStyle(fontSize = 22.sp, fontWeight = FontWeight.Bold))},
            text = {

                Column {
                    Spacer(modifier = Modifier.height(28.dp))

                    Row(Modifier.fillMaxWidth()) {
                        TextField(
                            value = nuevoPesoEstado.peso,
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                            onValueChange = { viewModel?.cambioPeso(it) },
                             label = { Text("Peso (Kg)") },
                            colors = TextFieldDefaults.textFieldColors(
                                backgroundColor = Color.Transparent,
                                focusedIndicatorColor = MaterialTheme.colors.primary,
                                unfocusedIndicatorColor = MaterialTheme.colors.primary
                            )

                        )
                    }

                        elegirFecha(context = LocalContext.current) { fecha ->
                            fechaTexto = fecha
                            viewModel?.cambioFecha(fecha)

                        }

                    Text(text = fechaTexto, textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth()
                    )

                    



                }
            }

            )
    }


}


//Funcion que utilizamos para mostrar el DatePicker  y guardar el valor de la fecha en variable por parametro 'fecha'
@Composable
fun elegirFecha(
    context: Context, Fecha: (String) ->Unit ){

    val calendario = Calendar.getInstance()
    val ano:Int
    ano = calendario.get(Calendar.YEAR)
    val mes:Int
    mes = calendario.get(Calendar.MONTH)
    val mess : Int = mes
    val dia:Int
    dia = calendario.get(Calendar.DAY_OF_MONTH)

    calendario.time = Date()

    val fecha = remember { mutableStateOf("") }
    val fechaPicker = DatePickerDialog(
        context,
        { _: DatePicker, a:Int, m:Int, d:Int ->
            val fechaselec = "$d/${m + 1}/$a" //+1 al mes porque el DatePicker de kotlin cuenta los meses desde 0 hasta 11
            fecha.value = fechaselec
            Fecha(fechaselec)
        }, ano,mes,dia
    )

    var fechaText by remember { mutableStateOf("") }
    Column (
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        Spacer(modifier = Modifier.size(7.dp))
        
        Button(onClick = {
            fechaPicker.show()
            fechaText = fecha.value
        },shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = Color(0xFF2DB4CC)
            ),
            modifier = Modifier.fillMaxWidth()
        ){ Text(text = "Elegir fecha")
            Icon(Icons.Default.CalendarMonth, contentDescription = "Fecha")

        }
    }
}


