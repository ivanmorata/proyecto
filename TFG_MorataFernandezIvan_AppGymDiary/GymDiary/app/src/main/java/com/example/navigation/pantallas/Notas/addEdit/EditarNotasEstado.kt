package com.example.navigation.pantallas.Notas.addEdit

import android.net.Uri
import com.example.navigation.Repositorios.ConexionBBDD.Notas.Notas

data class EditarNotasEstado( //Son las variables que utilizaremos en el viewModel y luego en las pantallas
    val color:Int = 0,
    val titulo:String = "",
    val infoNota:String = "",
    val estadoNotaNueva: Boolean = false,
    val estadoNotaActualz: Boolean = false,
    val notaSeleccionado:Notas? = null,
    val imagenUri: Uri? = null,
    val videoUri: Uri? = null,
    val cargando: Boolean = false, //CircularProgressIndicator

)


