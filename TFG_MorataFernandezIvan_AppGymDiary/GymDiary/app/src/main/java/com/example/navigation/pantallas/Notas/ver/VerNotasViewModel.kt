package com.example.navigation.pantallas.Notas.ver

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.navigation.Repositorios.ConexionBBDD.Notas.RepositoyNotas
import com.example.navigation.Repositorios.ConexionBBDD.Notas.ResultadoNotas
import com.example.navigation.pantallas.Notas.addEdit.EditarNotasEstado
import com.google.firebase.Timestamp
import kotlinx.coroutines.launch


class VerNotasViewModel(

        private val repositoyNotas: RepositoyNotas = RepositoyNotas()
): ViewModel() {

    //Estado
    var notaEstado by mutableStateOf(VerNotasEstado())
    var insertNotaEstado by mutableStateOf(EditarNotasEstado())
    private set

    //Datos firebae authentication
    val usuario = repositoyNotas.usuario()

    val usuarioIniciado:Boolean
    get() = repositoyNotas.usuarioIiniciado()

     val idUsuario:String
    get()= repositoyNotas.obtenerIdUsuario()

    val nombre = repositoyNotas.nombre()



    //Funciones para que nos devuelva el idUsuario del email que pongamos para hacer el insert del envio de nota
     suspend fun obtenerIdUsuarioPorEmail(email: String): String? {
        return repositoyNotas.obtenerIdUsuarioPorEmail(email)
    }

    suspend fun emailExiste(email: String):Boolean{
        return repositoyNotas.emailExiste(email)
    }



    //Enviar nota
    fun EnviarNota(idEnvio:String,titulo:String,infoNota:String,color:Int,imagen: String, video:String) {
        if (usuarioIniciado) {
                repositoyNotas.insertEnviarNota(
                    idUsuario = idEnvio,
                    titulo = titulo,
                    infoNota = infoNota,
                    color = color,
                    creacion = Timestamp.now(),
                    imagenUrl = imagen,
                    videoUrl = video
                ) {
                    insertNotaEstado = insertNotaEstado.copy(estadoNotaNueva = it)
            }
        }
    }


    //
    fun cargarNotas(){
        if(usuarioIniciado){
            if (idUsuario.isNotBlank()){
        obtenerNotasdeUsuario(idUsuario)
        }
    }else  {
        notaEstado = notaEstado.copy(listaNotas = ResultadoNotas.Error(
            throwable = Throwable(message = "Usuario no ha iniciado sesion")
        ))
    }
    }

    private fun obtenerNotasdeUsuario(idUsuario:String) = viewModelScope.launch {
        repositoyNotas.obtenerNotasUsuario(idUsuario).collect{
            notaEstado = notaEstado.copy(listaNotas = it)
        }
    }



    fun deleteNota(idNota:String) = repositoyNotas.deleteNota(idNota){
        notaEstado = notaEstado.copy(borrarNotaEstado = it)
    }

    fun cerrarSesion() = repositoyNotas.cerrarSesion()


}