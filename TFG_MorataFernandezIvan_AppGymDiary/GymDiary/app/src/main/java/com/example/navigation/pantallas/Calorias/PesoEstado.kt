package com.example.navigation.pantallas.Calorias

import com.example.navigation.Repositorios.ConexionBBDD.Notas.Peso
import com.example.navigation.Repositorios.ConexionBBDD.Peso.ResultadoPeso

//Control de los distintos estados contra el repositorias + los valores de los campos para viewModel-Pantalla
data class PesoEstado(
    val listaPeso: ResultadoPeso<List<Peso>> = ResultadoPeso.Cargando(),
    val pesoMostrar : List<Peso> = emptyList(),
    val borrarPesoEstado: Boolean = false
        )

data class NuevoPesoEstado(
    val fecha:String = "",
    val peso:String = "",
    val estadoPesoNueva: Boolean = false,
    val pesoSeleccionado: Peso? = null
)


