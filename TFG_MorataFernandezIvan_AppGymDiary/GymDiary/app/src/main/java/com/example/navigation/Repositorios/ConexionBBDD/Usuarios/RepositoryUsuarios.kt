package com.example.navigation.Repositorios.ConexionBBDD.Usuarios

import com.example.navigation.Repositorios.ConexionBBDD.Notas.Usuarios
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class RepositoryUsuarios {

    //Firebase authentication
    val usuario: FirebaseUser? = Firebase.auth.currentUser
    fun usuarioiniciado(): Boolean = Firebase.auth.currentUser != null
    fun obtenerIdUsuario(): String = Firebase.auth.currentUser?.uid.orEmpty()

    //Collection usuarios para la insercion
    private val ListaUsuarios : CollectionReference = Firebase.firestore.collection("usuarios")


    suspend fun iniciarSesion(email: String, contra: String, login: (Boolean) -> Unit) =
        withContext(Dispatchers.IO) {
            Firebase.auth
                //inicio sesion en app si coincide email y contraseña con firebase
                .signInWithEmailAndPassword(email, contra)
                .addOnCompleteListener { inicioSesion ->
                    if (inicioSesion.isSuccessful) {
                        login(true)
                    } else {
                        login(false)
                    }
                }.await()
        }

    suspend fun crearUsuario(email: String, contra: String, login: (Boolean) -> Unit) =
        withContext(Dispatchers.IO) {
            Firebase.auth
                //registrar usuario en Firebase Authentication
                .createUserWithEmailAndPassword(email, contra)
                .addOnCompleteListener() {
                    if (it.isSuccessful) {
                        //directamente iniciamos sesion en la app al registrarnos correctamente
                        login(true)
                    } else {
                        login(false)
                    }
                }.await()
        }


    //Insertamos el nombre de usuario(email) y el idUsuario en la collection "usuarios" de firebase
    fun insertarUsuario(idUsuario: String, email:String, Completado: (Boolean) -> Unit){
        val idDocumento = ListaUsuarios.document().id
        val usuario = Usuarios(idUsuario,email)
        ListaUsuarios.document(idDocumento).set(usuario).addOnCompleteListener{ insert ->
            Completado.invoke(insert.isSuccessful)

        }
    }

}