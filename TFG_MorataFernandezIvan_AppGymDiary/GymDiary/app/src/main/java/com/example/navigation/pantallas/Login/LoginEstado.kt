package com.example.navigation.pantallas.Login

data class LoginEstado(

    //Textfields de registro y login
    val nombre:String = "",
    val contra:String = "",
    val nombreRegistro:String = "",
    val contraRegistro:String = "",
    val confirmarContra:String = "",

    //Controlar los mensajes de error
    var RegistroError:String = "",
    val LoginError:String = "",


    val cargando: Boolean = false, //CircularProgressIndicator
    val LoginCoreccto:Boolean = false, // entrar app

    )
