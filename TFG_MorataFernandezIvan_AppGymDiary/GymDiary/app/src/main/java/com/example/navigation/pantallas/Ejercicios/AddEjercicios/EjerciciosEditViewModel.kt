package com.example.navigation.pantallas.Ejercicios.AddEjercicios

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.navigation.Repositorios.ConexionBBDD.Ejercicio.EjerciciosRepository
import com.example.navigation.Repositorios.ConexionBBDD.Ejercicio.Ejercicio
import com.example.navigation.Repositorios.ConexionBBDD.Ejercicio.ResultadoDatos
import com.example.navigation.pantallas.VerEjercicios.EjerciciosEstado
import com.google.firebase.auth.FirebaseUser
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.util.*
import javax.inject.Inject

@HiltViewModel
class EjerciciosEditViewModel

@Inject
constructor(
    private val ejerciciosRepository: EjerciciosRepository,
    verEjericios: SavedStateHandle


): ViewModel() {

    //Controles los estados de la clase estado desde la variable
    private val _estado: MutableState<EjerciciosEstado> = mutableStateOf(EjerciciosEstado())
    val estado: State<EjerciciosEstado>
        get() = _estado


    //Firebase authentication

    private val usuarioIniciado:Boolean
    get() = ejerciciosRepository.usuarioIniciado()


    private val usuario: FirebaseUser?
        get() = ejerciciosRepository.usuario()


    //obtener ejercicios
    init {
        verEjericios.get<String>("id")?.let { id ->
            obtenerEjercicios(id)
        }
    }

    //Insertar, actualizar, eliminar
    fun insertEjercicio(nombreEjercicio: String, InfoEjercicio: String,series: String, imagen:String, diaSemana:String) {
        if (usuarioIniciado) {
            val ejercicio = Ejercicio(
                idUsuario = usuario!!.uid,
                id = UUID.randomUUID().toString(), //generaremos un id random para cada ejercicio
                imagen = imagen,//
                nombreEjercicio = nombreEjercicio,
                infoEjercicio = InfoEjercicio,
                diaSemana = diaSemana,
                series = series,
                color = 2
            )

            ejerciciosRepository.insertEjercicios(ejercicio)
        }

    }



    fun updateEjercicio(nombreEjercicio: String,InfoEjercicio: String,series: String, imagen:String, diaSemana:String){
        if(estado.value.ejercicio == null){
            _estado.value = EjerciciosEstado(error = "Ha ocurrido un error inesperado")
            return
        }

        val actualiza = estado.value.ejercicio!!.copy(
            nombreEjercicio = nombreEjercicio,
            infoEjercicio = InfoEjercicio,
            series = series,
            imagen = imagen,
            diaSemana = diaSemana
        )

        ejerciciosRepository.updateEjercicio(actualiza.id,actualiza)
    }


    private fun obtenerEjercicios( IdEjercicio:String){
        ejerciciosRepository.recuperarEjercicio(IdEjercicio).onEach { resultado ->
            when(resultado){
                is ResultadoDatos.Cargando -> {
                    _estado.value = EjerciciosEstado(true)
                }
                is ResultadoDatos.Error -> {
                    _estado.value = EjerciciosEstado(error = resultado.mensaje ?: "Ha ocurrido un error inesperado")
                }
                is ResultadoDatos.Correcto -> {
                    _estado.value = EjerciciosEstado(ejercicio = resultado.info)
                }
            }
        }.launchIn(viewModelScope)
    }




}