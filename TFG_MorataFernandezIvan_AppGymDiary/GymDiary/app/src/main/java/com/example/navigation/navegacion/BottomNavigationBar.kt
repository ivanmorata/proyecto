package com.example.navigation.funciones

import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.navigation.NavController
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import com.example.navigation.navegacion.Destination

//Funcion donde controlamos el Bottom de navegacion de la parte inferior de la pantalla
@Composable
fun BottomNavigationBar(
    navController: NavController,
    lista: List<Destination>
){

    val ruta = rutaActual(navController)

    BottomNavigation() {
        lista.forEach { pantallas ->

            BottomNavigationItem(
                icon = { Icon(imageVector = pantallas.icono , contentDescription = pantallas.nombrePag) },
                label = { Text( pantallas.nombrePag)},
                selected = ruta == pantallas.ruta ,
                onClick = {
                          navController.navigate(pantallas.ruta){
                              popUpTo(navController.graph.findStartDestination().id){
                                  saveState = true
                              }
                              launchSingleTop = true
                          }
                },
                alwaysShowLabel = false
            )

        }
    }

}

@Composable
 fun rutaActual(navController: NavController) : String?{
    val navegacionAtras by navController.currentBackStackEntryAsState()
    return navegacionAtras?.destination?.route
}