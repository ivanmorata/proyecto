package com.example.navigation.pantallas.VerEjercicios

data class ejerciciosImagenes(
    val nombreEjercicio: String,
    val link:String
)

val listaEjerciciosLink = listOf(

    //Pecho
    ejerciciosImagenes("Press banca","https://i.blogs.es/5b4d69/bench-press-1/1366_2000.jpg") ,
    ejerciciosImagenes("Aperturas máquina","https://i.blogs.es/3082c6/apertura/1366_2000.jpg"),
    ejerciciosImagenes("Aperturas con mancuerna","https://i.blogs.es/5c0b8f/aperturas/1366_2000.jpg"),
    ejerciciosImagenes("Cruce de poleas", "https://i.blogs.es/6ab47e/cruce2/1366_2000.jpg"),

    //Hombro
    ejerciciosImagenes("Press militar","https://i.blogs.es/6485db/pres-hombro/1366_2000.jpg"),
    ejerciciosImagenes("Elevaciones laterales","https://i.blogs.es/7ad688/ejercicio21-3/450_1000.webp") ,
    ejerciciosImagenes("Pájaros con mancuerna","https://i.blogs.es/0889c9/pajaro1/450_1000.webp"),
    ejerciciosImagenes("Elevaciones frontales","https://i.blogs.es/d85006/elevaciones1/450_1000.webp"),
    ejerciciosImagenes("Encogimientos","https://i.blogs.es/6b1d85/encogimiento1/450_1000.webp") ,


    //Espalda
    ejerciciosImagenes("Remo con barra","https://i.blogs.es/99990c/remo-barra/1366_2000.jpg"),
    ejerciciosImagenes("Remo en T","https://i.blogs.es/0c65a1/remot1/450_1000.webp") ,
    ejerciciosImagenes("Remo con mancuernas","https://i.blogs.es/ff532c/remo/450_1000.webp"),
    ejerciciosImagenes("Remo en polea","https://i.blogs.es/209555/remo1/450_1000.webp"),
    ejerciciosImagenes("Jalón al pecho","https://i.blogs.es/c56863/poleaalpecho1/450_1000.webp"),
    ejerciciosImagenes("Dominadas","https://www.fisioterapiaconmueve.com/wp-content/uploads/2019/11/dominada.jpg") ,

    //Brazo y abs
     ejerciciosImagenes("Fondos","https://i.blogs.es/ce191f/dippingbis/1366_2000.jpg"),
     ejerciciosImagenes("Entensión tríceps","https://i.blogs.es/a4427d/triceps/450_1000.webp"),
     ejerciciosImagenes("Extensión trasnuca","https://i.blogs.es/aac9a0/triceps2/450_1000.webp"),
     ejerciciosImagenes("Entensión tríceps V","https://i.blogs.es/8f85b5/pull-over-polea/1366_2000.jpg") ,

     ejerciciosImagenes("Curl martillo","https://i.blogs.es/eeba20/martillo1/450_1000.webp"),
     ejerciciosImagenes("Curl en banco scott","https://i.blogs.es/f4b6c6/biceps1/450_1000.webp"),
     ejerciciosImagenes("Curl con barra","https://i.blogs.es/70fc57/bicep-curls-1/450_1000.webp"),
     ejerciciosImagenes("Curl Bíceps concentrado","https://i.blogs.es/3f2207/curl/450_1000.webp"),


    //Pierna
    ejerciciosImagenes("Prensa","https://i.blogs.es/7425be/prensa1/1366_2000.jpg"),
    ejerciciosImagenes("Sentadilla","https://i.blogs.es/93405c/sentadilla/450_1000.webp"),
    ejerciciosImagenes("Zancadas","https://i.blogs.es/c1905b/zancada1/1366_2000.jpg"),
    ejerciciosImagenes("Extensión de cuádriceps","https://i.blogs.es/c32e1e/guia1/1366_2000.jpg"),
    ejerciciosImagenes("Femoral sentado","https://i.blogs.es/58b557/curl-piernas/1366_2000.jpg"),
    ejerciciosImagenes("Peso muerto rumano","https://i.blogs.es/19909d/pesomuerto1/450_1000.webp"),
    ejerciciosImagenes("Femoral unilateral","https://i.blogs.es/eab7b2/curl2/1366_2000.jpg"),
    ejerciciosImagenes("Máquina aductores","https://i.blogs.es/19671d/abductor/1366_2000.jpg"),
    ejerciciosImagenes("Gemelo de pie","https://i.blogs.es/310362/elevacion2/1366_2000.jpg"),

    ejerciciosImagenes("Crunch abdominal","https://i.blogs.es/c68862/decline-crunch-1/1366_2000.jpg"),
    ejerciciosImagenes("Hipopresivos","https://i.blogs.es/7396c7/hipopresivo/450_1000.webp"),











)