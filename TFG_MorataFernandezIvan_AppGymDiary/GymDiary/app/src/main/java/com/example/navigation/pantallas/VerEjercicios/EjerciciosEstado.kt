package com.example.navigation.pantallas.VerEjercicios

import com.example.navigation.Repositorios.ConexionBBDD.Ejercicio.Ejercicio

data class EjerciciosEstado (
    val cargando: Boolean = false,
    val ejercicio: Ejercicio? = null,
    val ejercicioMostrar: List<Ejercicio> = emptyList(),
    val error: String = ""
        )

