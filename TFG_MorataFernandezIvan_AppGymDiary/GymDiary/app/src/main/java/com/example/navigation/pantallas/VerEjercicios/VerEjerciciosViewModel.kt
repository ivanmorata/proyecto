package com.example.navigation.pantallas.VerEjercicios


import android.content.Context
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.navigation.Repositorios.ConexionBBDD.Ejercicio.Ejercicio
import com.example.navigation.Repositorios.ConexionBBDD.Ejercicio.EjerciciosRepository
import com.example.navigation.Repositorios.ConexionBBDD.Ejercicio.ResultadoDatos
import com.google.firebase.Timestamp
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.util.*
import javax.inject.Inject

@HiltViewModel
class VerEjerciciosViewModel
@Inject
constructor(
    private val ejerciciosRepository: EjerciciosRepository,
): ViewModel(){

    //Datos usuario
    val usuario = ejerciciosRepository.usuario()

    val usuarioIniciado:Boolean
        get() = ejerciciosRepository.usuarioIniciado()

    private val idUsuario:String
        get()= ejerciciosRepository.obtenerIdUsuario()

    val nombre = ejerciciosRepository.nombre()


    // EjercicioEstado
    private val _estado: MutableState<EjerciciosEstado> = mutableStateOf(EjerciciosEstado())
    val estado: State<EjerciciosEstado> = _estado


    init { //se ejecuta al generar el viewmodel
        getEjerciciosList(idUsuario)
    }


    suspend fun obtenerIdUsuarioPorEmail(email: String): String? {
        return ejerciciosRepository.obtenerIdUsuarioPorEmail(email)
    }

    suspend fun emailExiste(email: String):Boolean{
        return ejerciciosRepository.emailExiste(email)
    }

    suspend fun verificarEjerciciosPendiente(context: Context){
        ejerciciosRepository.verificarEjercicioPendiente(idUsuario,context)
    }



    //Funciones para obtener y borrar ejercicios

    fun getEjerciciosList(idUsuario:String){ // obtener los ejercicios del usuario concreto
        ejerciciosRepository.obtenerEjerciciosUsuario(idUsuario).onEach { resultado ->
            when(resultado){
                is ResultadoDatos.Cargando -> {
                    _estado.value = EjerciciosEstado(cargando = true)
                }
                is ResultadoDatos.Correcto -> {
                    _estado.value = EjerciciosEstado(ejercicioMostrar = resultado.info ?: emptyList())
                }
                is ResultadoDatos.Error -> {
                    _estado.value = EjerciciosEstado(error = resultado.mensaje ?: "Ha ocurrido un error inesperado")
                }
            }

        }.launchIn(viewModelScope)
    }

    fun envioEjercicio(nombreEjercicio: String, InfoEjercicio: String,series: String,
                       imagen:String, diaSemana:String,idUsuario:String) {
        if (usuarioIniciado) {
            val ejercicio = Ejercicio(
                idUsuario = idUsuario,
                id = UUID.randomUUID().toString(), //generaremos un id random para cada ejercicio
                imagen = imagen,//
                nombreEjercicio = nombreEjercicio,
                infoEjercicio = InfoEjercicio,
                diaSemana = diaSemana,
                series = series,
                color = 2
            )

            ejerciciosRepository.insertEjercicios(ejercicio)
        }

    }

    fun updateColorEjercicio(idej: String, color:Int) {

        ejerciciosRepository.cambiarEjercicioPenddienteRealizado(
            color = color,
            idej = idej,
            timestamp = Timestamp.now()
        ) { success ->
           // estado = estado.copy(estadoNotaActualz = success)
        }
    }


    fun cargarEjercicios(){
        if(usuarioIniciado){
            if (idUsuario.isNotBlank()){
                getEjerciciosList(idUsuario)
            }
        }
    }

    fun deleteEjercicio(idEjercicio: String){ //funcion delete
        ejerciciosRepository.deleteEjercicio(idEjercicio)
    }



}

