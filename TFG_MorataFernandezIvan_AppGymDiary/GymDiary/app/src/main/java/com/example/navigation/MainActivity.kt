package com.example.navigation

import android.os.Build
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.rememberNavController
import com.example.navigation.funciones.BottomNavigationBar
import com.example.navigation.funciones.rutaActual
import com.example.navigation.navegacion.Destination
import com.example.navigation.navegacion.Navegacion
import com.example.navigation.navegacion.Destination.*
import com.example.navigation.ui.theme.NavigationTheme
import dagger.hilt.android.AndroidEntryPoint


@ExperimentalMaterialApi
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            NavigationTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    MainScreen()
                }
            }
        }
    }
}

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun MainScreen() {
    //Controlador de cambio de pantallas
    val navController = rememberNavController()

    //pantallas que apareceran en el botonbar
    val navegacionButtonBar = listOf(
        PantallaPerfil,
        PantallaVerNotas,
        PantallaEjercicios,
        PantallaPeso
    )

    //usamos esta variable para ver pantallas sin el botonbar : login,splash.
    val ruta = rutaActual(navController)

    Scaffold(
        bottomBar = {
            if(ruta != Destination.PantallaLogin.ruta && ruta !=Destination.PantallaRegistro.ruta
                && ruta !=Destination.PantallaSplaschScreen.ruta
                //&& ruta != Destination.PantallaFormCalorias.ruta
                ) {
                BottomNavigationBar(navController = navController, lista = navegacionButtonBar)
            }
        }
    ) {
        //Llamamos a Navegacion y comienza la aplicacion
        Navegacion(navController)
    }
}


