package com.example.navigation.Repositorios.ConexionBBDD.Ejercicio


sealed class ResultadoDatos<T> ( //manejaremos los datos de la bbdd
    val info: T? =  null,
    val mensaje: String?= null
){

    class Correcto<T>(info: T?) : ResultadoDatos<T>(info)

    class Error<T>(mensaje: String?, info: T? = null) : ResultadoDatos<T>(info,mensaje)

    class Cargando<T>(info: T? = null) : ResultadoDatos<T>(info)

}