package com.example.navigation.Repositorios.ConexionBBDD.Notas

import com.google.firebase.Timestamp

//colecciones y su tipo de dato de los datos que insertamos y actualizamos
data class Notas (
    val idUsuario: String = "",
    val titulo: String = "",
    val infoNota: String = "",
    val creacion: com.google.firebase.Timestamp = Timestamp.now(),
    val color: Int = 0,
    val idNota:String = "",
    var imagenUrl:String = "",
    var videoUrl:String = ""
)

data class Usuarios(
    val idUsuario: String = "",
    val email: String = ""

)

data class Peso (
    val idUsuario: String = "",
    val peso: String = "",
    val fecha:String = "",
    val creacion: com.google.firebase.Timestamp = Timestamp.now(),
    val idPeso:String = ""

)

