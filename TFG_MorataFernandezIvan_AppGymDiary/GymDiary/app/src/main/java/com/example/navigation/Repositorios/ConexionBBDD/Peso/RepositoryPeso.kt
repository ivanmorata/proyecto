package com.example.navigation.Repositorios.ConexionBBDD.Peso

import com.example.navigation.Repositorios.ConexionBBDD.Notas.Peso
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import java.lang.Exception

class RepositoryPeso() {

    fun usuario() = Firebase.auth.currentUser

    fun obtenerIdUsuario():String = Firebase.auth.currentUser?.uid.orEmpty()

    fun usuarioIiniciado(): Boolean= Firebase.auth.currentUser != null

    //Coleccion donde vamos a guardar nuestros pesos -> "Peso"
    private val ListaPeso : CollectionReference = Firebase.firestore.collection("Peso")



    //recuperar los pesos del usuario que este en la app
    fun obtenerPesosUsuario (idUsuario:String) : Flow<ResultadoPeso<List<Peso>>> = callbackFlow {

        var escucha: ListenerRegistration? = null

        try {
            escucha = ListaPeso.orderBy("creacion").whereEqualTo("idUsuario",idUsuario)
                .addSnapshotListener{ document,e ->
                    val res = if (document != null){
                        val peso = document.toObjects(Peso::class.java)
                        ResultadoPeso.Correcto(info = peso)
                    }else{
                        ResultadoPeso.Error(throwable = e?.cause)
                    }
                    trySend(res)

                }
        }catch (e: Exception){
            trySend(ResultadoPeso.Error(e?.cause))
        }

        awaitClose {
            escucha?.remove()
        }
    }




    //  Insercion y borrado de pesos

    fun insertPeso(idUsuario: String, peso:String, fecha:String,
                   creacion: com.google.firebase.Timestamp, Completado : (Boolean) -> Unit
    ){
        val idPeso = ListaPeso.document().id
        val pesoColleccion = Peso(idUsuario,peso,fecha,creacion,idPeso)

        ListaPeso.document(idPeso).set(pesoColleccion).addOnCompleteListener() { insert ->
            Completado(insert.isSuccessful)
        }
    }

    fun deletePeso(idPeso: String, Borrado:(Boolean) ->Unit){
        ListaPeso.document(idPeso).delete().addOnCompleteListener{
            Borrado(it.isSuccessful)
        }
    }


}