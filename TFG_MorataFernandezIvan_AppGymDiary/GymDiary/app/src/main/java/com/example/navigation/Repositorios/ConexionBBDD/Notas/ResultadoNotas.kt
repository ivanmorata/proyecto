package com.example.navigation.Repositorios.ConexionBBDD.Notas

sealed class ResultadoNotas<T>(
    val info: T? =  null,
    val throwable: Throwable? = null
){

    class Correcto<T>(info: T?) : ResultadoNotas<T>(info = info)

    class Error<T>(throwable: Throwable?):ResultadoNotas<T>(throwable = throwable)

    class Cargando<T>:ResultadoNotas<T>()

}

