package com.example.navigation.Repositorios.ConexionBBDD.Ejercicio

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.example.navigation.R
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await
import java.util.Calendar
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EjerciciosRepository
@Inject
constructor(
    //inyeccion collection "Ejercicios" en firebase
    private val ListaEjercicios: CollectionReference
) {

    //funciones firebase authentication
    fun usuario() = Firebase.auth.currentUser
    fun usuarioIniciado(): Boolean = Firebase.auth.currentUser != null
    fun obtenerIdUsuario():String = Firebase.auth.currentUser?.uid.orEmpty()
    fun nombre (): String = Firebase.auth.currentUser?.email.toString()


    suspend fun obtenerIdUsuarioPorEmail(email: String): String? {
        try {
            val ListaUsuarios = FirebaseFirestore.getInstance().collection("usuarios")

            val consulta = ListaUsuarios.whereEqualTo("email", email).limit(1).get().await()
            if (!consulta.isEmpty) {
                val document = consulta.documents.first()
                return document.getString("idUsuario")
            } else {
                return null
            }
        } catch (e: Exception) {
            return null
        }
    }


    //funcion que nos devolvera verdadero falso si el email que escriba el usuario al enviar su ejercicio esta registrado en Firebase Authentication o no no
    suspend fun emailExiste(email: String): Boolean {

        val emailEnvio = FirebaseAuth.getInstance().fetchSignInMethodsForEmail(email).await()

        return emailEnvio.signInMethods?.isNotEmpty() == true
    }


    //Obtener ejercicios del usuario para solo mostrar estos
    fun obtenerEjerciciosUsuario(idUsuario:String) : Flow<ResultadoDatos<List<Ejercicio>>> = callbackFlow {

        var estadoEscucha: ListenerRegistration? = null

        try {
            estadoEscucha =
                //la consulta de que coincida el idUsuario para mostrar ejercicios del usuario
                ListaEjercicios.orderBy("timestamp").whereEqualTo("idUsuario", idUsuario)
                    .addSnapshotListener { document, e ->
                        val respuesta = if (document != null) {
                            val ejercicios = document.toObjects(Ejercicio::class.java)
                            ResultadoDatos.Correcto(info = ejercicios)
                        } else {
                            ResultadoDatos.Error(mensaje = "Error al obtener los ejercicios-")
                        }
                        trySend(respuesta)
                    }
        }catch (e:java.lang.Exception){
            trySend(ResultadoDatos.Error( "Ha ocurrido un error al obtener tus ejercicios"))
        }
        awaitClose{
            estadoEscucha?.remove()
        }
    }

//Insercion de nuevos ejercicios
    fun insertEjercicios(ejercicio: Ejercicio){
        try {
            ListaEjercicios.document(ejercicio.id).set(ejercicio)
        //cada resgistro nuevo en la bbdd
        }catch (e:Exception){
                ResultadoDatos.Error<Ejercicio>(
                    mensaje =  "Ha ocurrido un error al agregar el ejercicio"
                )

        }
    }

    //funciones que usaremos para recuperar los datos de firestore para poder hacer update, para ello utilizamos el Id del ejercicio
    fun recuperarEjercicio(IdEjercicio:String): Flow<ResultadoDatos<Ejercicio>> = flow {

        try {
            emit(ResultadoDatos.Cargando<Ejercicio>())
            val ejercicio = ListaEjercicios.whereGreaterThanOrEqualTo("id",IdEjercicio).get().await()
                .toObjects(Ejercicio::class.java).first()
            emit(ResultadoDatos.Correcto(info = ejercicio))
        }catch (e:Exception){
            emit(
                ResultadoDatos.Error<Ejercicio>(
                    mensaje =  "Ha ocurrido un error"
                )
            )
        }
    }

    suspend fun verificarEjercicioPendiente(idUsuario:String,context: Context){
        val colorRealizado = 1 //Valor que utilizamos para saber que un ejercicio esta realizado

        val haceUnaSemana = Calendar.getInstance()
        haceUnaSemana.add(Calendar.DAY_OF_YEAR,-7)

        val ejerciciosIdUsuario = ListaEjercicios.whereEqualTo("idUsuario",idUsuario).get().await()

        for (ej in ejerciciosIdUsuario){
            val ejercicio = ej.toObject(Ejercicio::class.java)

            if(ejercicio.color == colorRealizado && ejercicio.timestamp.toDate() < haceUnaSemana.time){
                ejercicio.color = 2 //valor que utilizamos para que un ejercicio pase a pendiente
                updateEjercicio(ejercicio.id,ejercicio) //funcion de actualizacion
                mostrarNotificacion(context,"Ejercicios pendiente","Es el momento de superarte.\n¡Ánimo!")
            }
        }
    }


    //Actualizar el ejercicio, usaremos de nuevo el idejercicio para actualizar los campos
    fun updateEjercicio(idEjercicio: String, Ejercicio: Ejercicio){
            try {
                    val mapof = mapOf(
                        "nombreEjercicio" to Ejercicio.nombreEjercicio,
                        "infoEjercicio" to Ejercicio.infoEjercicio,
                        "series" to Ejercicio.series,
                        "imagen" to Ejercicio.imagen,
                        "diaSemana" to Ejercicio.diaSemana,
                        "color" to Ejercicio.color,
                    )

                ListaEjercicios.document(idEjercicio).update(mapof)
            }catch (e:Exception){
                ResultadoDatos.Error<Ejercicio>("Ha ocurrido un error al actualizar")
                }
            }


    fun cambiarEjercicioPenddienteRealizado(color: Int,
                                            idej: String, timestamp: Timestamp, Completado: (Boolean) -> Unit){

        val actualizDatos = hashMapOf<String,Any>(
            "color" to color,
            "timestamp" to timestamp

        )
        ListaEjercicios.document(idej).update(actualizDatos).addOnCompleteListener{ actualizado ->
            Completado(actualizado.isSuccessful)
        }
    }


    fun deleteEjercicio(idEjercicio: String){
            ListaEjercicios.document(idEjercicio).delete()
                }


    }


fun mostrarNotificacion(context: Context, titulo: String, mensaje: String) {
    val canal = "channel_id"
    val notificationId = 1

    val notificationBuilder = NotificationCompat.Builder(context, canal)
        .setSmallIcon(R.drawable.captura_de_pantalla_2023_06_17_a_las_19_28_40)
        .setContentTitle(titulo) // Título de la notificación
        .setContentText(mensaje) // Mensaje de la notificación
        .setPriority(NotificationCompat.PRIORITY_DEFAULT) //

    val notificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val channel = NotificationChannel(
            canal,
            "Channel Name",
            NotificationManager.IMPORTANCE_DEFAULT
        )
        notificationManager.createNotificationChannel(channel)
    }

    notificationManager.notify(notificationId, notificationBuilder.build())
}









