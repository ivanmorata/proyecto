package com.example.navigation.pantallas.VerEjercicios

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material.icons.filled.Save
import androidx.compose.material.icons.filled.Update
import androidx.compose.material.icons.outlined.FitnessCenter
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.navigation.navegacion.Destination

@Composable
fun editExercices(navController: NavController,
                  insertEjercicio: (String,String,String,String,String) ->Unit,
                  updateEjercicio: (String,String,String,String,String) ->Unit,
                  estado: EjerciciosEstado
                  ){

    // variables
    var nombreEjercicio by remember(estado.ejercicio?.nombreEjercicio){ mutableStateOf(estado.ejercicio?.nombreEjercicio ?: "")}
    var info by remember(estado.ejercicio?.infoEjercicio){ mutableStateOf(estado.ejercicio?.infoEjercicio ?: "")}
    var series by remember(estado.ejercicio?.series){ mutableStateOf(estado.ejercicio?.series ?: "")}
    var imagen by remember(estado.ejercicio?.imagen){ mutableStateOf(estado.ejercicio?.imagen ?: "")}
    var diaSemana by remember(estado.ejercicio?.diaSemana){ mutableStateOf(estado.ejercicio?.diaSemana ?: "")}

    val context = LocalContext.current

    var tituloCabecera by remember { mutableStateOf("")}

    //Lista con las imagenes
    val listaejercicioimagenes = remember { listaEjerciciosLink }

    val valorImagenes = remember {
        mutableStateOf<ejerciciosImagenes?>(null)
    }


    //dia de la semana
    val listaDiasSemana = remember { listadiasSemana }
    val valorDiaSemana = remember {
        mutableStateOf<diasSemana?>(null) }


    //Dialogo campos vacios
    var verDialogoCamposVacios by rememberSaveable { mutableStateOf(false) }
    DialogoCamposVacios(verDialogoCamposVacios,{verDialogoCamposVacios = false})

    val icono = if (estado.ejercicio?.id == null) {
        Icons.Default.Save //nuevo ej
    } else {
        Icons.Default.Update //actualizamos ej
    }

    //Parte superior + estructura
    Scaffold(
        floatingActionButton = {
            FloatingActionButton(
                onClick = {
                    if (estado.ejercicio?.id == null) {
                        if (series.isEmpty() or info.isEmpty() or imagen.isEmpty() or diaSemana.isEmpty())  {
                            verDialogoCamposVacios = true
                        } else {
                            insertEjercicio(nombreEjercicio, info, series, imagen,diaSemana)
                            Toast.makeText(context, "¡Nuevo ejercicio añadido!", Toast.LENGTH_SHORT)
                                .show()
                            navController.navigate(Destination.PantallaEjercicios.ruta)
                        }
                    }
                    else{
                        if (series.isEmpty() or info.isEmpty() or imagen.isEmpty() or diaSemana.isEmpty() or nombreEjercicio.isEmpty()){
                            verDialogoCamposVacios = true
                        } else {
                            updateEjercicio(nombreEjercicio, info, series, imagen,diaSemana)
                            Toast.makeText(context, "¡Ejercicio actualizado!", Toast.LENGTH_SHORT)
                                .show()
                            navController.navigate(Destination.PantallaEjercicios.ruta)
                        }

                    }
                },
                backgroundColor = Color(0xFF2DB4CC),
                modifier = Modifier.padding(bottom = 55.dp)
            ) {
                Icon(imageVector = icono , contentDescription = null )
            }

        }
    ) {

        //Pantalla nuevo ejercicio // edit ejercicio
    Box(
        modifier = Modifier.fillMaxSize().padding(16.dp)
    ) {
        Column(
            modifier = Modifier.fillMaxWidth()
        ) {


            Text(tituloCabecera,
                    style = TextStyle(
                        color = MaterialTheme.colors.primary,
                        fontSize = 26.sp,
                        fontWeight = FontWeight.Black,
                    ),
                modifier = Modifier.align(Alignment.CenterHorizontally)

            )

            Spacer(modifier = Modifier.height(16.dp))


            Icon(Icons.Outlined.FitnessCenter, contentDescription = "",
                modifier = Modifier
                    .size(120.dp)
                    .align(Alignment.CenterHorizontally)
            )


            Spacer(modifier = Modifier.height(16.dp))


           if (estado.ejercicio?.id == null) { //en funcion de un nuevo ejercico o ya existente listaDropDownEjercicios o OutlinedTextField
               listaDropDownEjercicios(ejerciciosImagenes = listaejercicioimagenes,
                   valorSelec = valorImagenes.value, onvalorcambiado = { ej ->
                       valorImagenes.value = ej
                       imagen = ej?.link ?: ""
                   }, valorej = nombreEjercicio)

               nombreEjercicio = valorImagenes.value?.nombreEjercicio ?: ""

           }else{
               OutlinedTextField(
                   modifier = Modifier
                       .fillMaxWidth()
                       .padding(bottom = 8.dp),
                   value = nombreEjercicio,
                   onValueChange = {
                       if (it.length <= 30){
                           nombreEjercicio = it }},
                   label = {
                       Text(text = "Título ejercicio")
                   }
               )
           }


            Spacer(modifier = Modifier.height(16.dp))

            OutlinedTextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 8.dp),
                value = series,
                onValueChange = {
                    if (it.length <= 2){
                    series = it }},
                label = {
                    Text(text = "Series")
                },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
            )

            Spacer(modifier = Modifier.height(16.dp))


            listaDropDownDiasSemana(diasSemanaLista = listaDiasSemana ,
                diaSemana = valorDiaSemana.value,{ dia ->
                valorDiaSemana.value = dia
                diaSemana = dia?.dia ?: ""

            }, valordia = diaSemana)

            Spacer(modifier = Modifier.height(16.dp))


            OutlinedTextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .padding(bottom = 8.dp),
                value = info,
                onValueChange = {
                    if (it.length <= 300){
                    info = it }},
                label = {
                    Text(text = "Descripción del ejercicio ( peso movido, repeticiones, etc.)")
                }
            )
            Spacer(modifier = Modifier.height(56.dp))

        }

        if(estado.error.isNotBlank()){
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp),
                text = estado.error,
                style = TextStyle(
                    color = Color.Red,
                    textAlign = TextAlign.Center
                )
            )
        }


        if (estado.cargando){
            CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
        } else if(estado.ejercicio?.id == null){
            tituloCabecera = "Añadir nuevo ejercicio"
        }else{
            tituloCabecera = "Actualizar ejercicio"

        }
    }}
}


@Composable
fun DialogoCamposVacios(ver:Boolean, aceptar: () -> Unit){
    if (ver) {
        AlertDialog(
            onDismissRequest = { aceptar() },
            confirmButton = {
                TextButton(onClick = { aceptar() }) {
                    Text(text = "Aceptar")
                }
            },
            text = { Text(text = "Debes rellenar todos las campos") },
            title = { Text(text = "Error")}

            )
    }
}


//DropdownMenu para los dias de la semana y los ejercicios
@Composable
fun listaDropDownDiasSemana(
    diasSemanaLista: List<diasSemana>,
    diaSemana: diasSemana?,
    ValorCambiado: (diasSemana?) -> Unit,
    valordia:String
) {
    var expandir by remember { mutableStateOf(false)}

    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        value = valordia,
        onValueChange = {
            valordia},
        readOnly = true,
        label = { Text("Día de la semana del ejercicio") },
        trailingIcon = {
            IconButton(onClick = { expandir = true }) {
                Icon(Icons.Default.ArrowDropDown, contentDescription = "")
            }
        }
    )

    DropdownMenu(
        expanded = expandir,
        onDismissRequest = { expandir = false }
    ) {
        diasSemanaLista.forEach { dia ->
            DropdownMenuItem(onClick = {
                ValorCambiado(dia)
                expandir = false
            }) {
                Text(text = dia.dia)
            }
        }
    }
}

data class diasSemana(
    val dia:String,
    val sigla:String
)

val listadiasSemana = listOf(
    diasSemana("Lunes","L"),
    diasSemana("Martes","M"),
    diasSemana("Miércoles","X"),
    diasSemana("Jueves","J"),
    diasSemana("Viernes","V"),
    diasSemana("Sábado","S"),
    diasSemana("Domingo","D"),

    )

@Composable
fun listaDropDownEjercicios(
    ejerciciosImagenes: List<ejerciciosImagenes>,
    valorSelec : ejerciciosImagenes?,
    onvalorcambiado: (ejerciciosImagenes?) -> Unit,
    valorej:String

){
    var expandir by remember { mutableStateOf(false)}

    OutlinedTextField(
        modifier = Modifier.fillMaxWidth(),
        value = valorej,
        onValueChange = {valorej},
        readOnly = true,
        label = { Text("Ejercicio") },
        trailingIcon = {
            IconButton(onClick = { expandir = true }) {
                Icon(Icons.Default.ArrowDropDown, contentDescription = "Expandir")
            }
        }
    )

    DropdownMenu(
        expanded = expandir,
        onDismissRequest = { expandir = false }
    ) {
        ejerciciosImagenes.forEach { ejercicio ->
            DropdownMenuItem(onClick = {
                onvalorcambiado(ejercicio)
                expandir = false
            }) {
                Text(text = ejercicio.nombreEjercicio)
            }
        }
    }
}